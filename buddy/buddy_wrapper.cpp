#include "bvec.h"
#include "buddy_wrapper.hpp"


namespace buddy {

    extern const bdd bdd_true = bddtrue;

    extern const bdd bdd_false = bddfalse;


    bdd_ptr::bdd_ptr(const bdd& val) : ptr(std::make_unique<bdd>(val)) {}

    bdd_ptr&
    bdd_ptr::operator=(bdd_ptr&& val) {
        ptr.swap(val.ptr);
        return *this;
    }


    bdd_ptr::bdd_ptr(bdd_ptr&& val) {
        ptr.swap(val.ptr);
    }

    const bdd&
    bdd_ptr::get_bdd() const {
        return *ptr;
    }

    bool
    bdd_ptr::is_true() const {
        return (*ptr == bddtrue) == 1;
    }

    bool
    bdd_ptr::is_false() const {
        return (*ptr == bddfalse) == 1;
    }

    bdd_ptr
    bdd_ptr::operator^(const bdd_ptr& other) const {
        return {*ptr ^ other.get_bdd()};
    }

    bdd_ptr
    bdd_ptr::operator&(const bdd_ptr& other) const {
        return {*ptr & other.get_bdd()};
    }

    int
    bdd_ptr::operator==(const bdd_ptr& other) const {
        return *ptr == other.get_bdd();
    }

    bdd_ptr
    bdd_ptr::operator!() {
        return {!(*ptr)};
    }

    int
    bdd_ptr::var() const {
        return bdd_var(*ptr);
    }

    bdd_ptr
    bdd_ptr::low() const {
        return {bdd_low(*ptr)};
    }

    bdd_ptr
    bdd_ptr::high() const {
        return {bdd_high(*ptr)};
    }

    bdd_ptr::~bdd_ptr() = default;

    bvec_ptr::bvec_ptr() : ptr(nullptr) {}

    bvec_ptr::bvec_ptr(const bvec& val) : ptr(std::make_unique<bvec>(val)) {}

    bvec_ptr&
    bvec_ptr::operator=(bvec_ptr&& val) {
        ptr.swap(val.ptr);
        return *this;
    }

    bvec_ptr::bvec_ptr(bvec_ptr&& val) {
        ptr.swap(val.ptr);
    }

    void
    bvec_ptr::set_bvec(const bvec& val) {
        ptr = std::make_unique<bvec>(val);
    }

    const bvec&
    bvec_ptr::get_bvec() const {
        return *ptr;
    }

    bvec&
    bvec_ptr::get_bvec() {
        return *ptr;
    }

    void
    bvec_ptr::set_true(int pos) {
        bvec& value = *ptr;
        value[pos] = bddtrue;
    }

    void
    bvec_ptr::set_false(int pos) {
        bvec& value = *ptr;
        value[pos] = bddfalse;
    }

    bool
    bvec_ptr::is_empty() const {
        return ptr->empty();
    }

    bool
    bvec_ptr::is_const() const {
        return bvec_isconst(*ptr) == 1;
    }

    int
    bvec_ptr::bvec_value() const {
        return bvec_val(*ptr);
    }

    bvec_ptr
    bvec_ptr::coerce(int bitnum) {
        return {bvec_coerce(bitnum, get_bvec())};
    }

    bdd_ptr
    bvec_ptr::operator[](int pos) const {
        return {(*ptr)[pos]};
    }

    bvec_ptr
    bvec_ptr::operator&(const bvec_ptr& right) const {
        return {bvec_map2(get_bvec(), right.get_bvec(), bdd_and)};
    }

    bvec_ptr
    bvec_ptr::operator^(const bvec_ptr& right) const {
        return {bvec_map2(get_bvec(), right.get_bvec(), bdd_xor)};
    }

    bvec_ptr
    bvec_ptr::operator|(const bvec_ptr& right) const {
        return {bvec_map2(get_bvec(), right.get_bvec(), bdd_or)};
    }

    bvec_ptr
    bvec_ptr::operator!() const {
        return {bvec_map1(get_bvec(), bdd_not)};
    }

    bvec_ptr
    bvec_ptr::operator<<(int con) const {
        return {bvec_shlfixed(get_bvec(), con, bddfalse)};
    }

    bvec_ptr
    bvec_ptr::operator<<(const bvec_ptr& right) const {
        return {bvec_shl(get_bvec(), right.get_bvec(), bddfalse)};
    }

    bvec_ptr
    bvec_ptr::operator>>(int con) const {
        return {bvec_shrfixed(get_bvec(), con, bddfalse)};
    }

    bvec_ptr
    bvec_ptr::operator>>(const bvec_ptr& right) const {
        return {bvec_shr(get_bvec(), right.get_bvec(), bddfalse)};
    }

    bvec_ptr
    bvec_ptr::operator+(const bvec_ptr& right) const {
        return {bvec_add(get_bvec(), right.get_bvec())};
    }

    bvec_ptr
    bvec_ptr::operator-(const bvec_ptr& right) const {
        return {bvec_sub(get_bvec(), right.get_bvec())};
    }

    bvec_ptr
    bvec_ptr::operator*(int con) const {
        return {bvec_mulfixed(get_bvec(), con)};
    }

    bvec_ptr
    bvec_ptr::operator*(const bvec_ptr& right) const {
        return {bvec_mul(get_bvec(), right.get_bvec())};
    }

    bdd_ptr
    bvec_ptr::operator<(const bvec_ptr& right) const {
        return {bvec_lth(get_bvec(), right.get_bvec())};
    }

    bdd_ptr
    bvec_ptr::operator<=(const bvec_ptr& right) const {
        return {bvec_lte(get_bvec(), right.get_bvec())};
    }

    bdd_ptr
    bvec_ptr::operator>(const bvec_ptr& right) const {
        return {bvec_gth(get_bvec(), right.get_bvec())};
    }

    bdd_ptr
    bvec_ptr::operator>=(const bvec_ptr& right) const {
        return {bvec_gte(get_bvec(), right.get_bvec())};
    }

    bdd_ptr
    bvec_ptr::operator==(const bvec_ptr& right) const {
        return {bvec_equ(get_bvec(), right.get_bvec())};
    }

    bdd_ptr
    bvec_ptr::operator!=(const bvec_ptr& right) const {
        return {bvec_neq(get_bvec(), right.get_bvec())};
    }

    bvec_ptr::~bvec_ptr() = default;

    void
    initBuddy()
    {
        bdd_init(1000, 1000);
        bdd_setvarnum(20);
    }

    void
    closeBuddy()
    {
        bdd_done();
    }

    int
    bitnum(const bvec& src) {
        return src.bitnum();
    }

    bvec_ptr bvec_true_buddy(int bitnum) {
        return {bvec_true(bitnum)};
    }

    bvec_ptr bvec_false_buddy(int bitnum) {
        return {bvec_false(bitnum)};
    }

    bvec_ptr bvec_con_buddy(int bitnum, int val) {
        return {bvec_con(bitnum, val)};
    }

    bvec_ptr bvec_var_buddy(int bitnum, int offset, int step) {
        return {bvec_var(bitnum, offset, step)};
    }

    bvec_ptr bvec_varvec_buddy(int bitnum, int *var) {
        return {bvec_varvec(bitnum, var)};
    }

    int bvec_div_buddy(const bvec_ptr& left, const bvec_ptr& right, bvec_ptr& res_ptr, bvec_ptr& rem_ptr) {
        bvec res{};
        bvec rem{};
        res_ptr.set_bvec(res);
        rem_ptr.set_bvec(rem);
        int result = bvec_div(left.get_bvec(), right.get_bvec(), res_ptr.get_bvec(), rem_ptr.get_bvec());
        return result;
    }

}
