#pragma once
#include <memory>

class bvec;
class bdd;


namespace buddy {

    class bdd_ptr {
        std::unique_ptr<bdd> ptr;

    public:

        bdd_ptr(bdd_ptr&& val);

        bdd_ptr&
        operator=(bdd_ptr&& val);

        bdd_ptr(const bdd& val);

        const bdd&
        get_bdd() const;

        bool
        is_true() const;

        bool
        is_false() const;

        bdd_ptr
        operator^(const bdd_ptr& other) const;

        bdd_ptr
        operator&(const bdd_ptr& other) const;

        int
        operator==(const bdd_ptr& other) const;

        bdd_ptr
        operator!();

        int
        var() const;

        bdd_ptr
        low() const;

        bdd_ptr
        high() const;

        ~bdd_ptr();
    };

    class bvec_ptr {

        std::unique_ptr<bvec> ptr;

    public:
        bvec_ptr();

        bvec_ptr(const bvec& val);

        bvec_ptr(bvec_ptr&& val);

        bvec_ptr&
        operator=(bvec_ptr&& val);

        void
        set_bvec(const bvec& val);

        const bvec&
        get_bvec() const;

        bvec&
        get_bvec();

        void
        set_true(int i);

        void
        set_false(int i);

        bool
        is_empty() const;

        bool
        is_const() const;

        int
        bvec_value() const;

        bvec_ptr
        coerce(int bitnum);

        bdd_ptr
        operator[](int pos) const;

        bvec_ptr operator&(const bvec_ptr& right) const;

        bvec_ptr operator^(const bvec_ptr& right) const;

        bvec_ptr operator|(const bvec_ptr& right) const;

        bvec_ptr operator!() const;

        bvec_ptr operator<<(int con) const;

        bvec_ptr operator<<(const bvec_ptr& right) const;

        bvec_ptr operator>>(int con) const;

        bvec_ptr operator>>(const bvec_ptr& right) const;

        bvec_ptr operator+(const bvec_ptr& right) const;

        bvec_ptr operator-(const bvec_ptr& right) const;

        bvec_ptr operator*(int con) const;

        bvec_ptr operator*(const bvec_ptr& right) const;

        bdd_ptr operator<(const bvec_ptr& right) const;

        bdd_ptr operator<=(const bvec_ptr& right) const;

        bdd_ptr operator>(const bvec_ptr& right) const;

        bdd_ptr operator>=(const bvec_ptr& right) const;

        bdd_ptr operator==(const bvec_ptr& right) const;

        bdd_ptr operator!=(const bvec_ptr& right) const;

        ~bvec_ptr();
    };

    extern const bdd bdd_true;

    extern const bdd bdd_false;

    void
    initBuddy();

    void
    closeBuddy();

    int
    bitnum(const bvec& src);

    bvec_ptr
    bvec_true_buddy(int bitnum);

    bvec_ptr
    bvec_false_buddy(int bitnum);

    bvec_ptr
    bvec_con_buddy(int bitnum, int val);

    bvec_ptr
    bvec_var_buddy(int bitnum, int offset, int step);

    bvec_ptr
    bvec_varvec_buddy(int bitnum, int *var);

    int
    bvec_div_buddy(const bvec_ptr& left, const bvec_ptr& right, bvec_ptr& res_ptr, bvec_ptr& rem_ptr);

}
