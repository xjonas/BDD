#include "catch.hpp"
#include "helpers.h"
#include "../cudd/bvec_cudd.h"
#include "../sylvan/bvec_sylvan.h"

TEST_CASE("Cudd unit tests") {
    using namespace cudd;
    Cudd manager;
    size_t size = 8U;
    uint8_t val = 42U;
    uint8_t con = 5U;
    uint8_t inv = ~val;
    uint8_t offset = 0U;
    uint8_t step = 2U;

    SECTION("Logic operators") {

        SECTION("Constants") {

            SECTION("Test cudd operation &/and") {
                Bvec x = Bvec::bvec_con(manager, size, val);
                Bvec y = Bvec::bvec_con(manager, size, val);
                Bvec res = x & y;
                Bvec res_inv = y & x;
                REQUIRE((res == y).IsOne());
                REQUIRE((res == x).IsOne());
                REQUIRE((res == res_inv).IsOne());
            }

            SECTION("Test cudd operation |/or") {
                Bvec x = Bvec::bvec_con(manager, size, val);
                Bvec y = Bvec::bvec_con(manager, size, inv);
                Bvec res = x | y;
                Bvec res_inv = y | x;
                REQUIRE((res == Bvec::bvec_true(manager, size)).IsOne());
                REQUIRE((y != x).IsOne());
                REQUIRE((res_inv == Bvec::bvec_true(manager, size)).IsOne());
            }

            SECTION("Test cudd operation ^/xor") {
                Bvec x = Bvec::bvec_con(manager, size, val);
                Bvec y = Bvec::bvec_con(manager, size, inv);
                Bvec res = x ^ y;
                Bvec res_inv = y ^ x;
                Bvec res_false = x ^ x;
                REQUIRE((res == Bvec::bvec_true(manager, size)).IsOne());
                REQUIRE((y != x).IsOne());
                REQUIRE((res_inv == Bvec::bvec_true(manager, size)).IsOne());
                REQUIRE((res_false == Bvec::bvec_false(manager, size)).IsOne());
            }

            SECTION("Test cudd operation !/not") {
                Bvec x = Bvec::bvec_con(manager, size, val);
                Bvec y = Bvec::bvec_con(manager, size, inv);
                Bvec res = !x;
                Bvec res_inv = !y;
                REQUIRE((res == y).IsOne());
                REQUIRE((res_inv == x).IsOne());
                REQUIRE((y != x).IsOne());
                res = !res;
                res_inv = !res_inv;
                REQUIRE((res == x).IsOne());
                REQUIRE((res_inv == y).IsOne());
            }

            SECTION("Test left/right shift operations") { //fix
                val = 44U;
                uint8_t bitshift = 2U;
                uint8_t l_shifted_val = val << bitshift;
                uint8_t r_shifted_val = val >> bitshift;
                Bvec bvec = Bvec::bvec_con(manager, size, val);
                Bvec l_shifted = Bvec::bvec_con(manager, size, l_shifted_val);
                Bvec r_shifted = Bvec::bvec_con(manager, size, r_shifted_val);
                Bvec shifting = Bvec::bvec_con(manager, size, bitshift);
                Bvec res = bvec << shifting;
                REQUIRE((res == l_shifted).IsOne());
                REQUIRE(res.bvec_val() == l_shifted_val);
                /*res = res >> shifting;
                REQUIRE((res == bvec).IsOne());
                REQUIRE(res.bvec_val() == val);*/

                Bvec fixed_res = bvec << bitshift;
                REQUIRE((fixed_res == l_shifted).IsOne());
                REQUIRE(fixed_res.bvec_val() == l_shifted_val);
                fixed_res = fixed_res >> bitshift;
                REQUIRE((fixed_res == bvec).IsOne());
                REQUIRE(fixed_res.bvec_val() == val);

                /*res = bvec >> shifting;
                REQUIRE((res == r_shifted).IsOne());
                REQUIRE(res.bvec_val() == r_shifted_val);
                res = res << shifting;
                REQUIRE((res == bvec).IsOne());
                REQUIRE(res.bvec_val() == val);*/

                fixed_res = bvec >> bitshift;
                REQUIRE((fixed_res == r_shifted).IsOne());
                REQUIRE(fixed_res.bvec_val() == r_shifted_val);
                fixed_res = fixed_res << bitshift;
                REQUIRE(fixed_res.bvec_val() == val);
                REQUIRE((fixed_res == bvec).IsOne());
            }
        }

        SECTION("Variables") {

            SECTION("Test cudd operation &/and") {
                Bvec x = Bvec::bvec_var(manager, size, offset, step);
                Bvec y = Bvec::bvec_var(manager, size, offset + 1U, step);
                Bvec res = x & y;
                Bvec res_inv = y & x;
                REQUIRE(!x.bvec_isConst());
                REQUIRE(!y.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(res_inv.bvec_val() == 0U);
                REQUIRE((res == res_inv).IsOne());
            }

            SECTION("Test cudd operation |/or") {
                Bvec x = Bvec::bvec_var(manager, size, offset, step);
                Bvec y = Bvec::bvec_var(manager, size, offset + 1U, step);
                Bvec res = x | y;
                Bvec res_inv = y | x;
                REQUIRE(!x.bvec_isConst());
                REQUIRE(!y.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(res_inv.bvec_val() == 0U);
                REQUIRE((res == res_inv).IsOne());
            }

            SECTION("Test cudd operation ^/xor") {
                Bvec x = Bvec::bvec_var(manager, size, offset, step);
                Bvec y = Bvec::bvec_var(manager, size, offset + 1U, step);
                Bvec res = x ^ y;
                Bvec res_inv = y ^ x;
                REQUIRE(!x.bvec_isConst());
                REQUIRE(!y.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(!res_inv.bvec_isConst());
                REQUIRE(res_inv.bvec_val() == 0U);
            }

            SECTION("Test cudd operation !/not") {
                Bvec x = Bvec::bvec_var(manager, size, offset, step);
                Bvec y = Bvec::bvec_var(manager, size, offset + 1U, step);
                Bvec res = !x;
                Bvec res_inv = !y;
                REQUIRE(!x.bvec_isConst());
                REQUIRE(!y.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(!res_inv.bvec_isConst());
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(res_inv.bvec_val() == 0U);
                REQUIRE((!res_inv == y).IsOne());
                REQUIRE((!res == x).IsOne());
            }

            SECTION("Test left/right shift operations") { //fix
                uint8_t bitshift = 2U;
                Bvec x = Bvec::bvec_var(manager, size, offset, step);
                Bvec y = Bvec::bvec_var(manager, size, offset + 1U, step);
                Bvec res = x << y;
                Bvec res_fixed = x << bitshift;
                REQUIRE(!x.bvec_isConst());
                REQUIRE(!y.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(!res_fixed.bvec_isConst());
                REQUIRE(res_fixed.bvec_val() == 0U);
                //res = res >> y;
                //REQUIRE((res == x).IsOne());
                //res_fixed = res_fixed >> bitshift;
                //REQUIRE((res_fixed == x).IsOne());
            }
        }
    }

    SECTION("Tests arithmetic operators") {

        SECTION("Constants") {

            SECTION("Test +") {
                Bvec bvec = Bvec::bvec_con(manager, size, val);
                Bvec small = Bvec::bvec_con(manager, size, con);
                Bvec res = bvec + small;
                Bvec res_inv = small + bvec;
                REQUIRE((res == res_inv).IsOne());
                REQUIRE(res.bvec_val() == res_inv.bvec_val());
                REQUIRE(res.bvec_val() == (val + con));
                REQUIRE((res == Bvec::bvec_con(manager, size, val + con)).IsOne());
            }

            SECTION("Test -") {
                Bvec bvec = Bvec::bvec_con(manager, size, val);
                Bvec small = Bvec::bvec_con(manager, size, con);
                Bvec res = bvec - small;
                Bvec added = res + small;
                REQUIRE(res.bvec_val() == (val - con));
                REQUIRE((bvec == added).IsOne());
                REQUIRE((small == (added - res)).IsOne());
                REQUIRE((res == Bvec::bvec_con(manager, size, val - con)).IsOne());
            }

            SECTION("Test * bitvector") {
                Bvec bvec = Bvec::bvec_con(manager, size, val);
                Bvec small = Bvec::bvec_con(manager, size, con);
                Bvec res = bvec * small;
                Bvec res_inv = small * bvec;
                REQUIRE((res == res_inv).IsOne());
                REQUIRE(res.bvec_val() == res_inv.bvec_val());
                REQUIRE(res.bvec_val() == (val * con));
                REQUIRE((res == Bvec::bvec_con(manager, size + size, val * con)).IsOne());
            }

            SECTION("Test * constant") {
                Bvec bvec = Bvec::bvec_con(manager, size, val);
                Bvec res = bvec * con;
                REQUIRE(res.bvec_val() == (val * con));
                REQUIRE((res == Bvec::bvec_con(manager, size, val * con)).IsOne());

            }

            SECTION("Test unsigned div") {
                Bvec bvec = Bvec::bvec_con(manager, size, val);
                Bvec div = Bvec::bvec_con(manager, size, con);
                Bvec res(manager);
                Bvec rem(manager);
                Bvec::bvec_div(bvec, div, res, rem);
                REQUIRE(res.bvec_val() == (val / con));
                REQUIRE((res == Bvec::bvec_con(manager, size, val / con)).IsOne());
                REQUIRE(rem.bvec_val() == (val % con));
                REQUIRE((rem == Bvec::bvec_con(manager, size, val % con)).IsOne());
            }

            SECTION("Test signed div") {
                Bvec bvec = Bvec::bvec_con(manager, size, val);
                Bvec div = Bvec::bvec_ncon(manager, size, -5);
                REQUIRE(div.bvec_nval() == -5);
                Bvec res(manager);
                Bvec rem(manager);
                Bvec::bvec_sdiv(bvec, div, res, rem);
                REQUIRE(res.bvec_nval() == -8);
            }
        }

        SECTION("Variables") {

            SECTION("Test +") {
                Bvec x = Bvec::bvec_var(manager, size, offset, step);
                Bvec y = Bvec::bvec_con(manager, size, con);
                Bvec res = x + y;
                Bvec inv = y + x;
                REQUIRE((res == inv).IsOne());
                BDD expected = (x == Bvec::bvec_con(manager, size, 2U));
                BDD result = (res == Bvec::bvec_con(manager, size, 7U));
                BDD miter = result.Xnor(expected);

                REQUIRE(miter.IsOne());
                REQUIRE(!x.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_isConst());
                REQUIRE(y.bvec_val() == 5U);
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(inv.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(!inv.bvec_isConst());
            }

            SECTION("Test -") {
                Bvec x = Bvec::bvec_var(manager, size, offset, step);
                Bvec y = Bvec::bvec_con(manager, size, con);
                Bvec res = x - y;
                BDD expected = (x == Bvec::bvec_con(manager, size, 10U));
                BDD result = (res == Bvec::bvec_con(manager, size, 5U));
                BDD miter = result.Xnor(expected);

                REQUIRE(miter.IsOne());
                REQUIRE(!x.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_isConst());
                REQUIRE(y.bvec_val() == 5U);
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
            }

            SECTION("Test * bitvector") {
                Bvec x = Bvec::bvec_var(manager, size, offset, step);
                Bvec y = Bvec::bvec_con(manager, size, con);
                Bvec res = (y * x).bvec_coerce(size);
                Bvec inv = (x * y).bvec_coerce(size);
                BDD expected = (x == Bvec::bvec_con(manager, size, 2U));
                BDD result = (res == Bvec::bvec_con(manager, size, 10U));
                BDD miter = result.Xnor(expected);

                REQUIRE(miter.IsOne());
                REQUIRE((res == inv).IsOne());
                REQUIRE(!x.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_isConst());
                REQUIRE(y.bvec_val() == 5U);
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(inv.bvec_val() == 0U);
                REQUIRE(!inv.bvec_isConst());

            }

            SECTION("Test * constant") {
                Bvec x = Bvec::bvec_var(manager, size, offset, step);
                Bvec res = x * con;
                BDD expected = (x == Bvec::bvec_con(manager, size, 2U));
                BDD result = (res == Bvec::bvec_con(manager, size, 10U));
                BDD miter = result.Xnor(expected);

                REQUIRE(miter.IsOne());
                REQUIRE(!x.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
            }

            SECTION("Test unsigned div") {
                Bvec x = Bvec::bvec_var(manager, size, offset, step);
                Bvec y = Bvec::bvec_con(manager, size, con);
                Bvec res(manager);
                Bvec rem(manager);
                Bvec::bvec_div(x, y, res, rem);
                BDD expected = (x >= Bvec::bvec_con(manager, size, 10U) & x < Bvec::bvec_con(manager, size, 15U));
                BDD result = (res == Bvec::bvec_con(manager, size, 2U));
                BDD miter = result.Xnor(expected);

                REQUIRE(miter.IsOne());
                REQUIRE(!x.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_isConst());
                REQUIRE(y.bvec_val() == 5U);
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(rem.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(!rem.bvec_isConst());
            }

            /*SECTION("Test signed div") { //fix
                Bvec bvec_ = Bvec::bvec_con(manager, size, val);
                Bvec div = Bvec::bvec_con(manager, size, -5);
                REQUIRE(div.bvec_val() == -5);
                Bvec res(manager);
                Bvec rem(manager);
                Bvec::bvec_sdiv(bvec_, div, res, rem);
            }*/

        }
    }

    SECTION("Test compare operators") {
        SECTION("Constants") {

            SECTION("Testing unsigned lth, lte operators") {
                Bvec small = Bvec::bvec_con(manager, size, val);
                Bvec big = Bvec::bvec_con(manager, size, inv);
                REQUIRE((small < big).IsOne());
                REQUIRE(!(big < small).IsOne());
                REQUIRE((small <= big).IsOne());
                REQUIRE(!(big <= small).IsOne());
                REQUIRE((small <= small).IsOne());
                REQUIRE(!(small < small).IsOne());
            }

            SECTION("Testing unsigned gth, gte operators") {
                Bvec small = Bvec::bvec_con(manager, size, val);
                Bvec big = Bvec::bvec_con(manager, size, inv);
                REQUIRE(!(small > big).IsOne());
                REQUIRE((big > small).IsOne());
                REQUIRE(!(small >= big).IsOne());
                REQUIRE((big >= small).IsOne());
                REQUIRE((small >= small).IsOne());
                REQUIRE(!(small > small).IsOne());
            }

            SECTION("Testing signed lth, lte operators") {
                Bvec small = Bvec::bvec_ncon(manager, size, -84);
                Bvec big = Bvec::bvec_ncon(manager, size, -42);
                REQUIRE(small.bvec_nval() == -84);
                REQUIRE(big.bvec_nval() == -42);
                BDD res = small < big;
                REQUIRE(res.IsOne());
                res = small <= big;
                REQUIRE(res.IsOne());
                res = big <= small;
                REQUIRE(!res.IsOne());
                res = big < small;
                REQUIRE(!res.IsOne());
                res = small > small;
                REQUIRE(!res.IsOne());
                res = small >= small;
                REQUIRE(res.IsOne());
            }

            SECTION("Testing signed gth, gte operators") {
                Bvec small = Bvec::bvec_ncon(manager,size, -84);
                Bvec big = Bvec::bvec_ncon(manager,size, -42);
                REQUIRE(small.bvec_nval() == -84);
                REQUIRE(big.bvec_nval() == -42);
                BDD res = small < big;
                REQUIRE(res.IsOne());
                res = small <= big;
                REQUIRE(res.IsOne());
                res = big <= small;
                REQUIRE(!res.IsOne());
                res = big < small;
                REQUIRE(!res.IsOne());
                res = small > small;
                REQUIRE(!res.IsOne());
                res = small >= small;
                REQUIRE(res.IsOne());
            }

            SECTION("Testing (non)equality operator") {
                Bvec small = Bvec::bvec_con(manager, size, val);
                Bvec big = Bvec::bvec_con(manager, size, inv);
                REQUIRE(!(small == big).IsOne());
                REQUIRE((small != big).IsOne());
                REQUIRE((big == big).IsOne());
                REQUIRE((small == small).IsOne());
                REQUIRE(!(small != small).IsOne());
                REQUIRE(!(big != big).IsOne());
            }
        }

        SECTION("Variables") {
            uint8_t con = 5U;
            SECTION("Testing unsigned lth, lte operators") {
                Bvec x = Bvec::bvec_var(manager, size, offset, step);
                Bvec y = Bvec::bvec_con(manager, size, con);
                BDD exp = (x < y);
                BDD res = x < y;
                BDD miter = res.Xnor(exp);
                REQUIRE(miter.IsOne());

                res = x <= y;
                exp = (x <= y);
                miter = res.Xnor(exp);
                REQUIRE(miter.IsOne());

                res = x > y;
                exp = (x > y);
                miter = res.Xnor(exp);
                REQUIRE(miter.IsOne());

                res = x >= y;
                exp = (x >= y);
                miter = res.Xnor(exp);
                REQUIRE(miter.IsOne());
            }

            /*SECTION("Testing signed lth, lte operators") {
                Bvec x = Bvec::bvec_con(manager, size, -84);
                Bvec y = Bvec::bvec_con(manager, size, -42);
                BDD res = x < y;
                REQUIRE(res.IsOne());
                res = x <= y;
                REQUIRE(res.IsOne());
                res = y <= x;
                REQUIRE(!res.IsOne());
                res = y < x;
                REQUIRE(!res.IsOne());
                res = x > x;
                REQUIRE(!res.IsOne());
                res = x >= x;
                REQUIRE(res.IsOne());
            }*/

            /*SECTION("Testing signed gth, gte operators") {
                Bvec small = Bvec::bvec_con(manager, size, -84);
                Bvec big = Bvec::bvec_con(manager, size, -42);
                BDD res = small < big;
                REQUIRE(res.IsOne());
                res = small <= big;
                REQUIRE(res.IsOne());
                res = big <= small;
                REQUIRE(!res.IsOne());
                res = big < small;
                REQUIRE(!res.IsOne());
                res = small > small;
                REQUIRE(!res.IsOne());
                res = small >= small;
                REQUIRE(res.IsOne());
            }*/

            SECTION("Testing equality operator") {
                Bvec x = Bvec::bvec_var(manager, size, offset, step);
                Bvec y = Bvec::bvec_con(manager, size, con);
                BDD exp = (x == y);
                BDD res = x == y;
                BDD inv = y == x;
                REQUIRE(res == inv);
                BDD miter = res.Xnor(exp);
                REQUIRE(miter.IsOne());

                exp = (x != y);
                res = x != y;
                inv = y != x;
                REQUIRE(res == inv);
                miter = res.Xnor(exp);
                REQUIRE(miter.IsOne());

            }
        }

    }
}

//----------------------------------------------------------------------------

TEST_CASE("Sylvan unit tests") {
    using namespace sylvan;
    using namespace helpers;
    initSylvan();
    uint8_t val = 42U;
    uint8_t con = 5U;
    uint8_t inv = ~val;
    size_t size = 8U;
    uint8_t offset = 0U;
    uint8_t step = 2U;

    SECTION("Logic operators") {

        SECTION("Constants") {

            SECTION("Test sylvan operation &/and") {
                Bvec x = Bvec::bvec_con(size, val);
                Bvec y = Bvec::bvec_con(size, val);
                Bvec res = x & y;
                Bvec res_inv = y & x;
                REQUIRE((res == y).isOne());
                REQUIRE((res == x).isOne());
                REQUIRE((res == res_inv).isOne());
            }

            SECTION("Test sylvan operation ^/xor") {
                Bvec x = Bvec::bvec_con(size, val);
                Bvec y = Bvec::bvec_con(size, inv);
                Bvec res = x ^ y;
                Bvec res_inv = y ^ x;
                Bvec res_false = x ^ x;
                REQUIRE((res == Bvec::bvec_true(size)).isOne());
                REQUIRE((y != x).isOne());
                REQUIRE((res_inv == Bvec::bvec_true(size)).isOne());
                REQUIRE((res_false == Bvec::bvec_false(size)).isOne());
            }

            SECTION("Test sylvan operation |/or") {
                Bvec x = Bvec::bvec_con(size, val);
                Bvec y = Bvec::bvec_con(size, inv);
                Bvec res = x | y;
                Bvec res_inv = y | x;
                REQUIRE((res == Bvec::bvec_true(size)).isOne());
                REQUIRE((y != x).isOne());
                REQUIRE((res_inv == Bvec::bvec_true(size)).isOne());
            }


            SECTION("Test sylvan operation !/not") {
                Bvec x = Bvec::bvec_con(size, val);
                Bvec y = Bvec::bvec_con(size, inv);
                Bvec res = !x;
                Bvec res_inv = !y;
                REQUIRE((res == y).isOne());
                REQUIRE((res_inv == x).isOne());
                REQUIRE((x != y).isOne());
                res = !res;
                res_inv = !res_inv;
                REQUIRE((res == x).isOne());
                REQUIRE((res_inv == y).isOne());
            }

            SECTION("Test left/right shift operations") {
                val = 44U;
                uint8_t bitshift = 2U;
                uint8_t l_shifted_val = val << bitshift;
                uint8_t r_shifted_val = val >> bitshift;
                Bvec bvec = Bvec::bvec_con(size, val);
                Bvec l_shifted = Bvec::bvec_con(size, l_shifted_val);
                Bvec r_shifted = Bvec::bvec_con(size, r_shifted_val);
                Bvec shifting = Bvec::bvec_con(size, bitshift);
                Bvec res = bvec << shifting;
                REQUIRE((res == l_shifted).isOne());
                REQUIRE(res.bvec_val() == l_shifted_val);
                /*res = res >> shifting;
                REQUIRE((res == bvec).isOne());
                REQUIRE(res.bvec_val() == val);*/

                Bvec fixed_res = bvec << bitshift;
                REQUIRE((fixed_res == l_shifted).isOne());
                REQUIRE(fixed_res.bvec_val() == l_shifted_val);
                fixed_res = fixed_res >> bitshift;
                REQUIRE((fixed_res == bvec).isOne());
                REQUIRE(fixed_res.bvec_val() == val);

                /*res = bvec >> shifting;
                REQUIRE((res == r_shifted).isOne());
                REQUIRE(res.bvec_val() == r_shifted_val);
                res = res << shifting;
                REQUIRE((res == bvec).isOne());
                REQUIRE(res.bvec_val() == val);*/

                fixed_res = bvec >> bitshift;
                REQUIRE((fixed_res == r_shifted).isOne());
                REQUIRE(fixed_res.bvec_val() == r_shifted_val);
                fixed_res = fixed_res << bitshift;
                REQUIRE(fixed_res.bvec_val() == val);
                REQUIRE((fixed_res == bvec).isOne());
            }
        }

        SECTION("Variables") {

            SECTION("Test operation &/and") {
                Bvec x = Bvec::bvec_var(size, offset, step);
                Bvec y = Bvec::bvec_var(size, offset + 1U, step);
                Bvec res = x & y;
                Bvec res_inv = y & x;
                REQUIRE(!x.bvec_isConst());
                REQUIRE(!y.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(res_inv.bvec_val() == 0U);
                REQUIRE((res == res_inv).isOne());
            }

            SECTION("Test operation |/or") {
                Bvec x = Bvec::bvec_var(size, offset, step);
                Bvec y = Bvec::bvec_var(size, offset + 1U, step);
                Bvec res = x | y;
                Bvec res_inv = y | x;
                REQUIRE(!x.bvec_isConst());
                REQUIRE(!y.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(res_inv.bvec_val() == 0U);
                REQUIRE((res == res_inv).isOne());
            }

            SECTION("Test operation ^/xor") {
                Bvec x = Bvec::bvec_var(size, offset, step);
                Bvec y = Bvec::bvec_var(size, offset + 1U, step);
                Bvec res = x ^ y;
                Bvec res_inv = y ^ x;
                REQUIRE(!x.bvec_isConst());
                REQUIRE(!y.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(!res_inv.bvec_isConst());
                REQUIRE(res_inv.bvec_val() == 0U);
            }

            SECTION("Test operation !/not") {
                Bvec x = Bvec::bvec_var(size, offset, step);
                Bvec y = Bvec::bvec_var(size, offset + 1U, step);
                Bvec res = !x;
                Bvec res_inv = !y;
                REQUIRE(!x.bvec_isConst());
                REQUIRE(!y.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(!res_inv.bvec_isConst());
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(res_inv.bvec_val() == 0U);
                REQUIRE((!res_inv == y).isOne());
                REQUIRE((!res == x).isOne());
            }

            SECTION("Test left/right shift operations") { //fix
                val = 44U;
                uint8_t bitshift = 2U;
                Bvec x = Bvec::bvec_var(size, offset, step);
                Bvec y = Bvec::bvec_var(size, offset + 1U, step);
                Bvec res = x << y;
                Bvec res_fixed = x << bitshift;
                REQUIRE(!x.bvec_isConst());
                REQUIRE(!y.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(!res_fixed.bvec_isConst());
                REQUIRE(res_fixed.bvec_val() == 0U);
                //res = res >> bitshift;
                //REQUIRE((res == x).isOne());
                res_fixed = res_fixed >> bitshift;
                //REQUIRE((res_fixed == x).isOne());
            }
        }
    }

    SECTION("Test arithmetic operators") {

        SECTION("Constants") {

            SECTION("Test +") {
                Bvec bvec = Bvec::bvec_con(size, val);
                Bvec small = Bvec::bvec_con(size, con);
                Bvec res = bvec + small;
                Bvec res_inv = small + bvec;
                REQUIRE((res == res_inv).isOne());
                REQUIRE(res.bvec_val() == res_inv.bvec_val());
                REQUIRE(res.bvec_val() == (val + con));
                REQUIRE((res == Bvec::bvec_con(size, val + con)).isOne());
            }

            SECTION("Test -") {
                Bvec bvec = Bvec::bvec_con(size, val);
                Bvec small = Bvec::bvec_con(size, con);
                Bvec res = bvec - small;
                Bvec added = res + small;
                REQUIRE(res.bvec_val() == (val - con));
                REQUIRE((bvec == added).isOne());
                REQUIRE((small == (added - res)).isOne());
                REQUIRE((res == Bvec::bvec_con(size, val - con)).isOne());
            }

            SECTION("Test * bitvector") {
                Bvec bvec = Bvec::bvec_con(size, val);
                Bvec small = Bvec::bvec_con(size, con);
                Bvec res = bvec * small;
                Bvec res_inv = small * bvec;
                REQUIRE((res == res_inv).isOne());
                REQUIRE(res.bvec_val() == res_inv.bvec_val());
                REQUIRE(res.bvec_val() == (val * con));
                REQUIRE((res == Bvec::bvec_con(size + size, val * con)).isOne());
            }

            SECTION("Test * constant") {
                Bvec bvec = Bvec::bvec_con(size, val);
                Bvec res = bvec * con;
                REQUIRE(res.bvec_val() == (val * con));
                REQUIRE((res == Bvec::bvec_con(size, val * con)).isOne());

            }

            SECTION("Test unsigned div") {
                Bvec bvec = Bvec::bvec_con(size, val);
                Bvec div = Bvec::bvec_con(size, con);
                Bvec res{};
                Bvec rem{};
                Bvec::bvec_div(bvec, div, res, rem);
                REQUIRE(res.bvec_val() == (val / con));
                REQUIRE((res == Bvec::bvec_con(size, val / con)).isOne());
                REQUIRE(rem.bvec_val() == (val % con));
                REQUIRE((rem == Bvec::bvec_con(size, val % con)).isOne());
            }

            SECTION("Test signed div") {
                Bvec bvec = Bvec::bvec_con(size, val);
                Bvec div = Bvec::bvec_ncon(size, -5);
                REQUIRE(div.bvec_nval() == -5);
                Bvec res{};
                Bvec rem{};
                Bvec::bvec_sdiv(bvec, div, res, rem);
                REQUIRE(res.bvec_nval() == -8);
            }
        }
        SECTION("Variables") {

            SECTION("Test +") {
                Bvec x = Bvec::bvec_var(size, offset, step);
                Bvec y = Bvec::bvec_con(size, con);
                Bvec res = x + y;
                Bvec inv = y + x;
                REQUIRE((res == inv).isOne());
                Bdd expected = (x == Bvec::bvec_con(size, 2U));
                Bdd result = (res == Bvec::bvec_con(size, 7U));
                Bdd miter = result.Xnor(expected);

                REQUIRE(miter.isOne());
                REQUIRE(!x.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_isConst());
                REQUIRE(y.bvec_val() == 5U);
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(inv.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(!inv.bvec_isConst());
            }

            SECTION("Test -") {
                Bvec x = Bvec::bvec_var(size, offset, step);
                Bvec y = Bvec::bvec_con(size, con);
                Bvec res = x - y;
                Bdd expected = (x == Bvec::bvec_con(size, 10U));
                Bdd result = (res == Bvec::bvec_con(size, 5U));
                Bdd miter = result.Xnor(expected);

                REQUIRE(miter.isOne());
                REQUIRE(!x.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_isConst());
                REQUIRE(y.bvec_val() == 5U);
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
            }

            SECTION("Test * bitvector") {
                Bvec x = Bvec::bvec_var(size, offset, step);
                Bvec y = Bvec::bvec_con(size, con);
                Bvec res = (y * x).bvec_coerce(size);
                Bvec inv = (x * y).bvec_coerce(size);
                Bdd expected = (x == Bvec::bvec_con(size, 2U));
                Bdd result = (res == Bvec::bvec_con(size, 10U));
                Bdd miter = result.Xnor(expected);

                REQUIRE(miter.isOne());
                REQUIRE((res == inv).isOne());
                REQUIRE(!x.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_isConst());
                REQUIRE(y.bvec_val() == 5U);
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(inv.bvec_val() == 0U);
                REQUIRE(!inv.bvec_isConst());

            }

            SECTION("Test * constant") {
                Bvec x = Bvec::bvec_var(size, offset, step);
                Bvec res = x * con;
                Bdd expected = (x == Bvec::bvec_con(size, 2U));
                Bdd result = (res == Bvec::bvec_con(size, 10U));
                Bdd miter = result.Xnor(expected);

                REQUIRE(miter.isOne());
                REQUIRE(!x.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
            }

            SECTION("Test unsigned div") {
                Bvec x = Bvec::bvec_var(size, offset, step);
                Bvec y = Bvec::bvec_con(size, con);
                Bvec res{};
                Bvec rem{};
                Bvec::bvec_div(x, y, res, rem);
                Bdd expected = (x >= Bvec::bvec_con(size, 10U) & x < Bvec::bvec_con(size, 15U));
                Bdd result = (res == Bvec::bvec_con(size, 2U));
                Bdd miter = result.Xnor(expected);

                REQUIRE(miter.isOne());
                REQUIRE(!x.bvec_isConst());
                REQUIRE(x.bvec_val() == 0U);
                REQUIRE(y.bvec_isConst());
                REQUIRE(y.bvec_val() == 5U);
                REQUIRE(res.bvec_val() == 0U);
                REQUIRE(rem.bvec_val() == 0U);
                REQUIRE(!res.bvec_isConst());
                REQUIRE(!rem.bvec_isConst());
            }

            /*SECTION("Test signed div") { //fix
                Bvec x = Bvec::bvec_con(size, val);
                Bvec y = Bvec::bvec_con(size, -5);
                REQUIRE(y.bvec_val() == -5);
                Bvec res{};
                Bvec rem{};
                Bvec::bvec_sdiv(x, y, res, rem);
            }*/
        }
    }

    SECTION("Test compare operators") {

        SECTION("Constants") {

            SECTION("Testing unsigned lth, lte operators") {
                Bvec small = Bvec::bvec_con(size, val);
                Bvec big = Bvec::bvec_con(size, inv);
                REQUIRE((small < big).isOne());
                REQUIRE(!(big < small).isOne());
                REQUIRE((small <= big).isOne());
                REQUIRE(!(big <= small).isOne());
                REQUIRE((small <= small).isOne());
                REQUIRE(!(small < small).isOne());
            }

            SECTION("Testing unsigned gth, gte operators") {
                Bvec small = Bvec::bvec_con(size, val);
                Bvec big = Bvec::bvec_con(size, inv);
                REQUIRE(!(small > big).isOne());
                REQUIRE((big > small).isOne());
                REQUIRE(!(small >= big).isOne());
                REQUIRE((big >= small).isOne());
                REQUIRE((small >= small).isOne());
                REQUIRE(!(small > small).isOne());
            }

            SECTION("Testing signed lth, lte operators") {
                Bvec small = Bvec::bvec_ncon(size, -84);
                Bvec big = Bvec::bvec_ncon(size, -42);
                REQUIRE(small.bvec_nval() == -84);
                REQUIRE(big.bvec_nval() == -42);
                Bdd res = small < big;
                REQUIRE(res.isOne());
                res = small <= big;
                REQUIRE(res.isOne());
                res = big <= small;
                REQUIRE(!res.isOne());
                res = big < small;
                REQUIRE(!res.isOne());
                res = small > small;
                REQUIRE(!res.isOne());
                res = small >= small;
                REQUIRE(res.isOne());
            }

            SECTION("Testing signed gth, gte operators") {
                Bvec small = Bvec::bvec_ncon(size, -84);
                Bvec big = Bvec::bvec_ncon(size, -42);
                REQUIRE(small.bvec_nval() == -84);
                REQUIRE(big.bvec_nval() == -42);
                Bdd res = small < big;
                REQUIRE(res.isOne());
                res = small <= big;
                REQUIRE(res.isOne());
                res = big <= small;
                REQUIRE(!res.isOne());
                res = big < small;
                REQUIRE(!res.isOne());
                res = small > small;
                REQUIRE(!res.isOne());
                res = small >= small;
                REQUIRE(res.isOne());
            }

            SECTION("Testing (non)equality operator") {
                Bvec small = Bvec::bvec_con(size, val);
                Bvec big = Bvec::bvec_con(size, inv);
                REQUIRE(!(small == big).isOne());
                REQUIRE((small != big).isOne());
                REQUIRE((big == big).isOne());
                REQUIRE((small == small).isOne());
                REQUIRE(!(small != small).isOne());
                REQUIRE(!(big != big).isOne());
            }
        }

        SECTION("Variables") {
            uint8_t con = 5U;

            SECTION("Testing unsigned lth, lte operators") {
                Bvec x = Bvec::bvec_var(size, offset, step);
                Bvec y = Bvec::bvec_con(size, con);
                sylvan::Bdd exp = (x < Bvec::bvec_con(size, con));
                sylvan::Bdd res = x < y;
                sylvan::Bdd miter = res.Xnor(exp);
                REQUIRE(miter.isOne());

                res = x <= y;
                exp = (x <= y);
                miter = res.Xnor(exp);
                REQUIRE(miter.isOne());

                res = x > y;
                exp = (x > y);
                miter = res.Xnor(exp);
                REQUIRE(miter.isOne());

                res = x >= y;
                exp = (x >= y);
                miter = res.Xnor(exp);
                REQUIRE(miter.isOne());
            }

            /*SECTION("Testing signed lth, lte operators") {
                Bvec x = Bvec::bvec_con(size, -84);
                Bvec y = Bvec::bvec_con(size, -42);
                Bdd res = x < y;
                REQUIRE(res.isOne());
                res = x <= y;
                REQUIRE(res.isOne());
                res = y <= x;
                REQUIRE(!res.isOne());
                res = y < x;
                REQUIRE(!res.isOne());
                res = x > x;
                REQUIRE(!res.isOne());
                res = x >= x;
                REQUIRE(res.isOne());
            }*/

            SECTION("Testing equality operator") {
                Bvec x = Bvec::bvec_var(size, offset, step);
                Bvec y = Bvec::bvec_con(size, con);
                sylvan::Bdd exp = (x == y);
                sylvan::Bdd res = x == y;
                sylvan::Bdd inv = y == x;
                REQUIRE(res == inv);
                sylvan::Bdd miter = res.Xnor(exp);
                REQUIRE(miter.isOne());

                exp = (x != y);
                res = x != y;
                inv = y != x;
                REQUIRE(res == inv);
                miter = res.Xnor(exp);
                REQUIRE(miter.isOne());

            }
        }
    }

    closeSylvan();
}

