#include "catch.hpp"
#include "helpers.h"
#include "../cudd/bvec_cudd.h"
#include "../sylvan/bvec_sylvan.h"
#include "../buddy/buddy_wrapper.hpp"

using namespace helpers;


TEST_CASE("Construction tests") {
    init();
    Cudd manager;
    size_t size = 0U;

    SECTION("Test false bitvector") {
        size = 42U;
        cudd::Bvec bvec_cudd = cudd::Bvec::bvec_false(manager, size);

        sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_false(size);

        buddy::bvec_ptr bvec_buddy = buddy::bvec_false_buddy(size);

        REQUIRE(isEqual(bvec_cudd, bvec_sylvan, bvec_buddy));
    }


    SECTION("Test true bitvector") {
        size = 42U;
        cudd::Bvec bvec_cudd = cudd::Bvec::bvec_true(manager, size);

        sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_true(size);

        buddy::bvec_ptr bvec_buddy = buddy::bvec_true_buddy(size);

        REQUIRE(isEqual(bvec_cudd, bvec_sylvan, bvec_buddy));
    }

    SECTION("Test constant bitvector") {
        size_t val = 42U;
        size = 8U;
        cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
        sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
        buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);

        REQUIRE(isEqual(bvec_cudd, bvec_sylvan, bvec_buddy, val));

    }

    SECTION("Test copy constructors") {
        size_t val = 42U;
        size = 6U;
        cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
        cudd::Bvec cudd_copy (bvec_cudd);
        cudd::Bvec cudd_operator = bvec_cudd;
        REQUIRE((bvec_cudd == cudd_copy).IsOne());
        REQUIRE((bvec_cudd == cudd_operator).IsOne());

        sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
        sylvan::Bvec sylvan_copy (bvec_sylvan);
        sylvan::Bvec sylvan_operator = bvec_sylvan;
        REQUIRE((bvec_sylvan == sylvan_copy).isOne());
        REQUIRE((bvec_sylvan == sylvan_operator).isOne());

        buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);
        /*buddy::bvec_ptr buddy_copy (bvec_buddy);
        buddy::bvec_ptr buddy_operator = bvec_buddy;*/
        /*REQUIRE((bvec_buddy == buddy_copy).is_true());
        REQUIRE((bvec_buddy == buddy_operator).is_true());*/

        REQUIRE(isEqual(bvec_cudd, bvec_sylvan, bvec_buddy, val));

    }

    /*SECTION("Test bvec_var") {
        size = 8U;
        int offset = 1;
        int step = 2;
        cudd::Bvec bvec_cudd = cudd::Bvec::bvec_var(manager, size, offset, step);
        sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_var(size, offset, step);
        buddy::bvec_ptr bvec_buddy = buddy::bvec_var_buddy(size, offset, step);
    }

    SECTION("Test bvec_varvec") {
        std::vector<int> var {0, 1, 2, 3, 4, 5, 6, 7};
        size = var.size();
        cudd::Bvec bvec_cudd = cudd::Bvec::bvec_varvec(manager, size, var.data());
        sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_varvec(size, var.data());
        buddy::bvec_ptr bvec_buddy = bvec_varvec_buddy(size, var.data());
    }*/

    finish();
}


TEST_CASE("Manipulation tests") {
    init();
    Cudd manager;
    size_t size = 0U;

    SECTION("Test bitnum function") {
        size = 42U;
        cudd::Bvec cudd = cudd::Bvec::bvec_false(manager, size);

        sylvan::Bvec sylvan = sylvan::Bvec::bvec_false(size);

        buddy::bvec_ptr buddy = buddy::bvec_false_buddy(size);

        REQUIRE(cudd.bitnum() == sylvan.bitnum());
        REQUIRE(sylvan.bitnum() == buddy::bitnum(buddy.get_bvec()));
        REQUIRE(isEqual(cudd, sylvan, buddy));
    }

    SECTION("Test set and [] function") {
        size = 8U;
        size_t value = 42;
        cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, value);

        sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, value);

        buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, value);

        REQUIRE(bvec_cudd[3] == manager.bddOne());
        REQUIRE(bvec_cudd[1] == manager.bddOne());
        REQUIRE(bvec_cudd[0] == manager.bddZero());

        REQUIRE(bvec_sylvan[3] == sylvan::Bdd::bddOne());
        REQUIRE(bvec_sylvan[1] == sylvan::Bdd::bddOne());
        REQUIRE(bvec_sylvan[0] == sylvan::Bdd::bddZero());

        REQUIRE(bvec_buddy[3].is_true());
        REQUIRE(bvec_buddy[1].is_true());
        REQUIRE(bvec_buddy[0].is_false());

        REQUIRE(isEqual(bvec_cudd, bvec_sylvan, bvec_buddy));
    }

    SECTION("Test empty function") {
        size = 0U;
        cudd::Bvec bvec_cudd = cudd::Bvec::bvec_false(manager, size);
        sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_false(size);
        buddy::bvec_ptr bvec_buddy = buddy::bvec_false_buddy(size);

        REQUIRE(bvec_cudd.empty());
        REQUIRE(bvec_sylvan.empty());
        REQUIRE(bvec_buddy.is_empty());
    }

    SECTION("Test bvec_isConst") {
        size_t val = 42U;
        size = 8U;
        cudd::Bvec cudd_val = cudd::Bvec::bvec_con(manager, size, val);
        sylvan::Bvec sylvan_val = sylvan::Bvec::bvec_con(size, val);
        buddy::bvec_ptr buddy_val = buddy::bvec_con_buddy(size, val);

        REQUIRE(isEqual(cudd_val, sylvan_val, buddy_val));
        REQUIRE(cudd_val.bvec_isConst());
        REQUIRE(sylvan_val.bvec_isConst());
        REQUIRE(buddy_val.is_const());

        int offset = 0;
        int step = 2;
        cudd::Bvec cudd_var = cudd::Bvec::bvec_var(manager, size, offset, step);
        sylvan::Bvec sylvan_var = sylvan::Bvec::bvec_var(size, offset, step);
        buddy::bvec_ptr buddy_var = buddy::bvec_var_buddy(size, offset, step);

        REQUIRE(!cudd_var.bvec_isConst());
        REQUIRE(!sylvan_var.bvec_isConst());
        REQUIRE(!buddy_var.is_const());
        REQUIRE(isEqualVar(cudd_var, sylvan_var, buddy_var));
    }

    SECTION("Test bvec_val") {
        size_t val = 42U;
        size = 8U;
        cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
        sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
        buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);

        REQUIRE(isEqual(bvec_cudd, bvec_sylvan, bvec_buddy));

        int cudd_val = bvec_cudd.bvec_val();
        int sylvan_val = bvec_sylvan.bvec_val();
        int buddy_val = bvec_buddy.bvec_value();
        REQUIRE(cudd_val == sylvan_val);
        REQUIRE(cudd_val == buddy_val);
        REQUIRE(val == cudd_val);
    }

    finish();
}

TEST_CASE("Map functions tests") {
    init();
    Cudd manager;
    size_t size = 0U;

    SECTION("Map1") {
        size = 8U;
        cudd::Bvec cudd = cudd::Bvec::bvec_false(manager, size);

        sylvan::Bvec sylvan = sylvan::Bvec::bvec_false(size);

        buddy::bvec_ptr buddy = buddy::bvec_false_buddy(size);

        REQUIRE(isEqual(cudd, sylvan, buddy));

        cudd = !cudd;

        REQUIRE((cudd == cudd::Bvec::bvec_true(manager, size)).IsOne());
        REQUIRE(!isEqual(cudd, sylvan, buddy));

        sylvan = !sylvan;
        REQUIRE((sylvan == sylvan::Bvec::bvec_true(size)).isOne());
        REQUIRE(!isEqual(cudd, sylvan, buddy));

        buddy = !buddy;
        REQUIRE((buddy == buddy::bvec_true_buddy(size)).is_true());
        REQUIRE(isEqual(cudd, sylvan, buddy));
    }

    SECTION("Map2 test") {
        size = 8U;
        cudd::Bvec cudd = cudd::Bvec::bvec_false(manager, size);

        sylvan::Bvec sylvan = sylvan::Bvec::bvec_false(size);

        buddy::bvec_ptr buddy = buddy::bvec_false_buddy(size);

        REQUIRE(isEqual(cudd, sylvan, buddy));

        cudd = cudd * cudd;

        REQUIRE(cudd.bitnum() == (size + size));
        REQUIRE(!isEqual(cudd, sylvan, buddy));

        sylvan = sylvan * sylvan;
        REQUIRE(sylvan.bitnum() == (size + size));
        REQUIRE(!isEqual(cudd, sylvan, buddy));

        buddy = buddy * buddy;
        REQUIRE(buddy::bitnum(buddy.get_bvec()) == (size + size));
        REQUIRE(isEqual(cudd, sylvan, buddy));
    }

    SECTION("Map3 test") { //fix
        /*size = 8U;
        cudd::Bvec cudd = cudd::Bvec::bvec_false(manager, size);

        sylvan::Bvec sylvan = sylvan::Bvec::bvec_false(size);

        buddy::bvec_ptr buddy = buddy::bvec_false_buddy(size);

        REQUIRE(isEqual(cudd, sylvan, buddy.get_bvec()));

        cudd = cudd::Bvec::bvec_map3(cudd, cudd, cudd,
            [] (const BDD& f, const BDD& s, const BDD& t) {
                return (f & s) & t;
            });

        REQUIRE(isEqual(cudd, sylvan, buddy.get_bvec()));

        sylvan = sylvan::Bvec::bvec_map3(sylvan, sylvan, sylvan,
            [] (const sylvan::Bdd& f, const sylvan::Bdd& s, const sylvan::Bdd& t) {
                return (f & s) & t;
            });
        REQUIRE(isEqual(cudd, sylvan, buddy.get_bvec()));

        buddy = bvec_map3(buddy, buddy, buddy,
            [] (const bdd& f, const bdd& s, const bdd& t) {
                return (f & s) & t;
            });
        REQUIRE(isEqual(cudd, sylvan, buddy.get_bvec()));*/
    }

    finish();
}

TEST_CASE("Basic logic operations") {
    init();
    Cudd manager;
    size_t size = 8U;

    SECTION("Test constants") {
        uint8_t val = 42U;
        uint8_t inv = ~val;

        SECTION("Test operation &/and") {
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_inv = cudd::Bvec::bvec_con(manager, size, inv);
            cudd::Bvec cudd_false = bvec_cudd & cudd_inv;
            cudd::Bvec cudd_res = bvec_cudd & bvec_cudd;
            cudd::Bvec cudd_inv_res = cudd_inv & cudd_inv;
            REQUIRE((cudd_false == cudd::Bvec::bvec_false(manager, size)).IsOne());
            REQUIRE((cudd_res == bvec_cudd).IsOne());
            REQUIRE((cudd_inv_res == cudd_inv).IsOne());

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_inv = sylvan::Bvec::bvec_con(size, inv);
            sylvan::Bvec sylvan_false = bvec_sylvan & sylvan_inv;
            sylvan::Bvec sylvan_res = bvec_sylvan & bvec_sylvan;
            sylvan::Bvec sylvan_inv_res = sylvan_inv & sylvan_inv;
            REQUIRE((sylvan_false == sylvan::Bvec::bvec_false(size)).isOne());
            REQUIRE((sylvan_res == bvec_sylvan).isOne());
            REQUIRE((sylvan_inv_res == sylvan_inv).isOne());

            buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);
            buddy::bvec_ptr buddy_inv = buddy::bvec_con_buddy(size, inv);
            buddy::bvec_ptr buddy_false = bvec_buddy & buddy_inv;
            buddy::bvec_ptr buddy_res = bvec_buddy & bvec_buddy;
            buddy::bvec_ptr buddy_inv_res = buddy_inv & buddy_inv;
            REQUIRE((buddy_false == buddy::bvec_false_buddy(size)).is_true());
            REQUIRE((buddy_res == bvec_buddy).is_true());
            REQUIRE((buddy_inv_res == buddy_inv).is_true());

            REQUIRE(isEqual(cudd_inv, sylvan_inv, buddy_inv, inv));
            REQUIRE(isEqual(bvec_cudd, bvec_sylvan, bvec_buddy, val));
        }

        SECTION("Test operation |/or") {
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_inv = cudd::Bvec::bvec_con(manager, size, inv);
            cudd::Bvec cudd_true = bvec_cudd | cudd_inv;
            cudd::Bvec cudd_res = bvec_cudd | bvec_cudd;
            cudd::Bvec cudd_inv_res = cudd_inv | cudd_inv;
            REQUIRE((cudd_true == cudd::Bvec::bvec_true(manager, size)).IsOne());
            REQUIRE((cudd_res == bvec_cudd).IsOne());
            REQUIRE((cudd_inv_res == cudd_inv).IsOne());

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_inv = sylvan::Bvec::bvec_con(size, inv);
            sylvan::Bvec sylvan_true = bvec_sylvan | sylvan_inv;
            sylvan::Bvec sylvan_res = bvec_sylvan | bvec_sylvan;
            sylvan::Bvec sylvan_inv_res = sylvan_inv | sylvan_inv;
            REQUIRE((sylvan_true == sylvan::Bvec::bvec_true(size)).isOne());
            REQUIRE((sylvan_res == bvec_sylvan).isOne());
            REQUIRE((sylvan_inv_res == sylvan_inv).isOne());

            buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);
            buddy::bvec_ptr buddy_inv = buddy::bvec_con_buddy(size, inv);
            buddy::bvec_ptr buddy_true = bvec_buddy | buddy_inv;
            buddy::bvec_ptr buddy_res = bvec_buddy | bvec_buddy;
            buddy::bvec_ptr buddy_inv_res = buddy_inv | buddy_inv;
            REQUIRE((buddy_true == buddy::bvec_true_buddy(size)).is_true());
            REQUIRE((buddy_res == bvec_buddy).is_true());
            REQUIRE((buddy_inv_res == buddy_inv).is_true());

            REQUIRE(isEqual(cudd_inv, sylvan_inv, buddy_inv, inv));
            REQUIRE(isEqual(bvec_cudd, bvec_sylvan, bvec_buddy, val));
        }

        SECTION("Test operation ^/xor") {
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_inv = cudd::Bvec::bvec_con(manager, size, inv);
            cudd::Bvec cudd_true = bvec_cudd ^ cudd_inv;
            cudd::Bvec cudd_false = bvec_cudd ^ bvec_cudd;
            REQUIRE((cudd_false == cudd::Bvec::bvec_false(manager, size)).IsOne());
            REQUIRE((cudd_true == cudd::Bvec::bvec_true(manager, size)).IsOne());

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_inv = sylvan::Bvec::bvec_con(size, inv);
            sylvan::Bvec sylvan_true = bvec_sylvan ^ sylvan_inv;
            sylvan::Bvec sylvan_false = bvec_sylvan ^ bvec_sylvan;
            REQUIRE((sylvan_true == sylvan::Bvec::bvec_true(size)).isOne());
            REQUIRE((sylvan_false == sylvan::Bvec::bvec_false(size)).isOne());

            buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);
            buddy::bvec_ptr buddy_inv = buddy::bvec_con_buddy(size, inv);
            buddy::bvec_ptr buddy_true = bvec_buddy ^ buddy_inv;
            buddy::bvec_ptr buddy_false = bvec_buddy ^ bvec_buddy;
            REQUIRE((buddy_true == buddy::bvec_true_buddy(size)).is_true());
            REQUIRE((buddy_false == buddy::bvec_false_buddy(size)).is_true());

            REQUIRE(isEqual(cudd_inv, sylvan_inv, buddy_inv, inv));
            REQUIRE(isEqual(bvec_cudd, bvec_sylvan, bvec_buddy, val));
        }

        SECTION("Test operation !/not") {
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_inv = cudd::Bvec::bvec_con(manager, size, inv);
            cudd::Bvec cudd_res = !bvec_cudd;
            cudd::Bvec cudd_inv_res = !cudd_inv;
            REQUIRE((cudd_res == cudd_inv).IsOne());
            REQUIRE((cudd_inv_res == bvec_cudd).IsOne());

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_inv = sylvan::Bvec::bvec_con(size, inv);
            sylvan::Bvec sylvan_res = !bvec_sylvan;
            sylvan::Bvec sylvan_inv_res = !sylvan_inv;
            REQUIRE((sylvan_res == sylvan_inv).isOne());
            REQUIRE((sylvan_inv_res == bvec_sylvan).isOne());

            buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);
            buddy::bvec_ptr buddy_inv = buddy::bvec_con_buddy(size, inv);
            buddy::bvec_ptr buddy_res = !bvec_buddy;
            buddy::bvec_ptr buddy_inv_res = !buddy_inv;
            REQUIRE((buddy_res == buddy_inv).is_true());
            REQUIRE((buddy_inv_res == bvec_buddy).is_true());

            REQUIRE(isEqual(cudd_inv, sylvan_inv, buddy_inv, inv));
            REQUIRE(isEqual(bvec_cudd, bvec_sylvan, bvec_buddy, val));
        }

        SECTION("Test left/right shift operations") { //fix
            uint8_t bitshift = 2U;
            uint8_t shift = val << bitshift;
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec shifted_cudd = cudd::Bvec::bvec_con(manager, size, shift);
            cudd::Bvec shifting_cudd = cudd::Bvec::bvec_con(manager, size, bitshift);
            cudd::Bvec cudd_res = bvec_cudd << shifting_cudd;
            cudd::Bvec cudd_fixed_res = bvec_cudd << bitshift;
            REQUIRE((cudd_res == shifted_cudd).IsOne());
            REQUIRE((cudd_fixed_res == shifted_cudd).IsOne());

            //cudd_res = cudd_res >> shifting_cudd;
            cudd_fixed_res = cudd_fixed_res >> bitshift;
            //REQUIRE((cudd_res == bvec_cudd).IsOne());
            REQUIRE((cudd_fixed_res == bvec_cudd).IsOne());

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec shifted_sylvan = sylvan::Bvec::bvec_con(size, shift);
            sylvan::Bvec shifting_sylvan = sylvan::Bvec::bvec_con(size, bitshift);
            sylvan::Bvec sylvan_res = bvec_sylvan << shifting_sylvan;
            sylvan::Bvec sylvan_fixed_res = bvec_sylvan << bitshift;
            REQUIRE((sylvan_res == shifted_sylvan).isOne());
            REQUIRE((sylvan_fixed_res == shifted_sylvan).isOne());

            //sylvan_res = sylvan_res >> shifting_sylvan;
            sylvan_fixed_res = sylvan_fixed_res >> bitshift;
            //REQUIRE((sylvan_res == bvec_sylvan).isOne());
            REQUIRE((sylvan_fixed_res == bvec_sylvan).isOne());

            buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);
            buddy::bvec_ptr shifted_buddy = buddy::bvec_con_buddy(size, shift);
            buddy::bvec_ptr shifting_buddy = buddy::bvec_con_buddy(size, bitshift);
            buddy::bvec_ptr buddy_res = bvec_buddy << shifting_buddy;
            buddy::bvec_ptr buddy_fixed_res = bvec_buddy << bitshift;
            REQUIRE((buddy_res == shifted_buddy).is_true());
            REQUIRE((buddy_fixed_res == shifted_buddy).is_true());

            //buddy_res = buddy_res >> shifting_buddy; //broken
            buddy_fixed_res = buddy_fixed_res >> bitshift;
            //REQUIRE((buddy_res == bvec_buddy).is_true());
            REQUIRE((buddy_fixed_res == bvec_buddy).is_true());

            REQUIRE(isEqual(shifted_cudd, shifted_sylvan, shifted_buddy, shift));
            REQUIRE(isEqual(bvec_cudd, bvec_sylvan, bvec_buddy, val));
        }
    }

//------------------------------------------------------------------------------------

    SECTION("Test variables") {
        uint8_t offset = 0U;
        uint8_t step = 2U;
        size = 5U;

        SECTION("Test operation &/and") {
            cudd::Bvec cuddX = cudd::Bvec::bvec_var(manager, size, offset, step);
            cudd::Bvec cuddY = cudd::Bvec::bvec_var(manager, size, offset + 1, step);
            cudd::Bvec cudd_res = cuddX & cuddY;
            cudd::Bvec cudd_inv = cuddY & cuddX;
            REQUIRE((cudd_res == cudd_inv).IsOne());

            sylvan::Bvec sylvanX = sylvan::Bvec::bvec_var(size, offset, step);
            sylvan::Bvec sylvanY = sylvan::Bvec::bvec_var(size, offset + 1, step);
            sylvan::Bvec sylvan_res = sylvanX & sylvanY;
            sylvan::Bvec sylvan_inv = sylvanY & sylvanX;
            REQUIRE((sylvan_res == sylvan_inv).isOne());

            buddy::bvec_ptr buddyX = buddy::bvec_var_buddy(size, offset, step);
            buddy::bvec_ptr buddyY = buddy::bvec_var_buddy(size, offset + 1, step);
            buddy::bvec_ptr buddy_res = buddyX & buddyY;
            buddy::bvec_ptr buddy_inv = buddyY & buddyX;
            REQUIRE((buddy_res == buddy_inv).is_true());

            REQUIRE(buddyX.bvec_value() == cuddX.bvec_val());
            REQUIRE(buddyX.bvec_value() == sylvanX.bvec_val());
            REQUIRE(buddyY.bvec_value() == cuddY.bvec_val());
            REQUIRE(buddyY.bvec_value() == sylvanY.bvec_val());
            REQUIRE(buddy_res.bvec_value() == cudd_res.bvec_val());
            REQUIRE(buddy_res.bvec_value() == sylvan_res.bvec_val());
            REQUIRE(buddyX.is_const() == cuddX.bvec_isConst());
            REQUIRE(buddyX.is_const() == sylvanX.bvec_isConst());
            REQUIRE(buddyY.is_const() == cuddY.bvec_isConst());
            REQUIRE(buddyY.is_const() == sylvanY.bvec_isConst());
            REQUIRE(buddy_res.is_const() == sylvan_res.bvec_isConst());
            REQUIRE(buddy_res.is_const() == cudd_res.bvec_isConst());
            REQUIRE(isEqualVar(cudd_res, sylvan_res, buddy_res));
        }

        SECTION("Test operation |/or") {
            cudd::Bvec cuddX = cudd::Bvec::bvec_var(manager, size, offset, step);
            cudd::Bvec cuddY = cudd::Bvec::bvec_var(manager, size, offset + 1, step);
            cudd::Bvec cudd_res = cuddX | cuddY;
            cudd::Bvec cudd_inv = cuddY | cuddX;
            REQUIRE((cudd_res == cudd_inv).IsOne());

            sylvan::Bvec sylvanX = sylvan::Bvec::bvec_var(size, offset, step);
            sylvan::Bvec sylvanY = sylvan::Bvec::bvec_var(size, offset + 1, step);
            sylvan::Bvec sylvan_res = sylvanX | sylvanY;
            sylvan::Bvec sylvan_inv = sylvanY | sylvanX;
            REQUIRE((sylvan_res == sylvan_inv).isOne());

            buddy::bvec_ptr buddyX = buddy::bvec_var_buddy(size, offset, step);
            buddy::bvec_ptr buddyY = buddy::bvec_var_buddy(size, offset + 1, step);
            buddy::bvec_ptr buddy_res = buddyX | buddyY;
            buddy::bvec_ptr buddy_inv = buddyY | buddyX;
            REQUIRE((buddy_res == buddy_inv).is_true());

            REQUIRE(buddyX.bvec_value() == cuddX.bvec_val());
            REQUIRE(buddyX.bvec_value() == sylvanX.bvec_val());
            REQUIRE(buddyY.bvec_value() == cuddY.bvec_val());
            REQUIRE(buddyY.bvec_value() == sylvanY.bvec_val());
            REQUIRE(buddy_res.bvec_value() == cudd_res.bvec_val());
            REQUIRE(buddy_res.bvec_value() == sylvan_res.bvec_val());
            REQUIRE(buddyX.is_const() == cuddX.bvec_isConst());
            REQUIRE(buddyX.is_const() == sylvanX.bvec_isConst());
            REQUIRE(buddyY.is_const() == cuddY.bvec_isConst());
            REQUIRE(buddyY.is_const() == sylvanY.bvec_isConst());
            REQUIRE(buddy_res.is_const() == sylvan_res.bvec_isConst());
            REQUIRE(buddy_res.is_const() == cudd_res.bvec_isConst());
            REQUIRE(isEqualVar(cudd_res, sylvan_res, buddy_res));
        }

        SECTION("Test operation ^/xor") {
            cudd::Bvec cuddX = cudd::Bvec::bvec_var(manager, size, offset, step);
            cudd::Bvec cuddY = cudd::Bvec::bvec_var(manager, size, offset + 1, step);
            cudd::Bvec cudd_res = cuddX ^ cuddY;
            cudd::Bvec cudd_inv = cuddY ^ cuddX;
            REQUIRE((cudd_res == cudd_inv).IsOne());

            sylvan::Bvec sylvanX = sylvan::Bvec::bvec_var(size, offset, step);
            sylvan::Bvec sylvanY = sylvan::Bvec::bvec_var(size, offset + 1, step);
            sylvan::Bvec sylvan_res = sylvanX ^ sylvanY;
            sylvan::Bvec sylvan_inv = sylvanY ^ sylvanX;
            REQUIRE((sylvan_res == sylvan_inv).isOne());

            buddy::bvec_ptr buddyX = buddy::bvec_var_buddy(size, offset, step);
            buddy::bvec_ptr buddyY = buddy::bvec_var_buddy(size, offset + 1, step);
            buddy::bvec_ptr buddy_res = buddyX ^ buddyY;
            buddy::bvec_ptr buddy_inv = buddyY ^ buddyX;
            REQUIRE((buddy_res == buddy_inv).is_true());

            REQUIRE(buddyX.bvec_value() == cuddX.bvec_val());
            REQUIRE(buddyX.bvec_value() == sylvanX.bvec_val());
            REQUIRE(buddyY.bvec_value() == cuddY.bvec_val());
            REQUIRE(buddyY.bvec_value() == sylvanY.bvec_val());
            REQUIRE(buddy_res.bvec_value() == cudd_res.bvec_val());
            REQUIRE(buddy_res.bvec_value() == sylvan_res.bvec_val());
            REQUIRE(buddyX.is_const() == cuddX.bvec_isConst());
            REQUIRE(buddyX.is_const() == sylvanX.bvec_isConst());
            REQUIRE(buddyY.is_const() == cuddY.bvec_isConst());
            REQUIRE(buddyY.is_const() == sylvanY.bvec_isConst());
            REQUIRE(buddy_res.is_const() == sylvan_res.bvec_isConst());
            REQUIRE(buddy_res.is_const() == cudd_res.bvec_isConst());
            REQUIRE(isEqualVar(cudd_res, sylvan_res, buddy_res));
        }

        SECTION("Test operation !/not") {
            cudd::Bvec cudd = cudd::Bvec::bvec_var(manager, size, offset, step);
            cudd::Bvec cudd_res = !cudd;
            REQUIRE((cudd == !cudd_res).IsOne());

            sylvan::Bvec sylvan = sylvan::Bvec::bvec_var(size, offset, step);
            sylvan::Bvec sylvan_res = !sylvan;
            REQUIRE((sylvan == !sylvan_res).isOne());

            buddy::bvec_ptr buddy = buddy::bvec_var_buddy(size, offset, step);
            buddy::bvec_ptr buddy_res = !buddy;
            REQUIRE((buddy == !buddy_res).is_true());

            REQUIRE(buddy.bvec_value() == cudd.bvec_val());
            REQUIRE(buddy.bvec_value() == sylvan.bvec_val());
            REQUIRE(buddy_res.bvec_value() == cudd_res.bvec_val());
            REQUIRE(buddy_res.bvec_value() == sylvan_res.bvec_val());
            REQUIRE(buddy.is_const() == cudd.bvec_isConst());
            REQUIRE(buddy.is_const() == sylvan.bvec_isConst());
            REQUIRE(buddy_res.is_const() == sylvan_res.bvec_isConst());
            REQUIRE(buddy_res.is_const() == cudd_res.bvec_isConst());
            REQUIRE(isEqualVar(cudd_res, sylvan_res, buddy_res));
        }

        SECTION("Test left/right shift operations") { //fix
            uint8_t bitshift = 2U;
            cudd::Bvec cuddX = cudd::Bvec::bvec_var(manager, size, offset, step);
            cudd::Bvec cuddY = cudd::Bvec::bvec_var(manager, size, offset + 1, step);
            cudd::Bvec cudd_res = cuddX << cuddY;
            cudd::Bvec cudd_inv = cuddY << cuddX;
            REQUIRE((cudd_res == cudd_inv).IsOne());
            cudd::Bvec cudd_fixed = cuddX << bitshift;
            //REQUIRE((cuddX == cudd_fixed >> bitshift).isOne());

            sylvan::Bvec sylvanX = sylvan::Bvec::bvec_var(size, offset, step);
            sylvan::Bvec sylvanY = sylvan::Bvec::bvec_var(size, offset + 1, step);
            sylvan::Bvec sylvan_res = sylvanX << sylvanY;
            sylvan::Bvec sylvan_inv = sylvanY << sylvanX;
            //REQUIRE((sylvan_res == sylvan_inv).isOne());
            sylvan::Bvec sylvan_fixed = sylvanX << bitshift;
            //REQUIRE((sylvanX == sylvan_fixed >> bitshift).isOne());

            buddy::bvec_ptr buddyX = buddy::bvec_var_buddy(size, offset, step);
            buddy::bvec_ptr buddyY = buddy::bvec_var_buddy(size, offset + 1, step);
            // bvec buddy_res = buddyX << buddyY;
            // bvec buddy_inv = buddyY << buddyX;
            //REQUIRE((buddy_res == buddy_inv).isOne());
            // bvec buddy_fixed = buddyX << bitshift;
            //REQUIRE((buddyX == buddy_fixed >> bitshift).isOne());

            // REQUIRE(isEqualVar(cudd_res, sylvan_res, buddy_res.get_bvec()));
            // REQUIRE(isEqualVar(cudd_fixed, sylvan_fixed, buddy_fixed.get_bvec()));
        }
    }

    finish();
}

TEST_CASE("Test arithmetic operations (+/-/*/div)") {
    init();
    Cudd manager;
    size_t size = 8U;

    SECTION("Testing constants") {
        uint8_t val = 42U;
        uint8_t inv = ~val;
        uint8_t con = 5U;

        SECTION("Test +") {
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_small = cudd::Bvec::bvec_con(manager, size, con);
            cudd::Bvec cudd_inv = cudd::Bvec::bvec_con(manager, size, inv);
            cudd::Bvec cudd_true = bvec_cudd + cudd_inv;
            cudd::Bvec cudd_res = bvec_cudd + cudd_small;
            cudd::Bvec cudd_inv_res = cudd_inv + cudd_small;
            REQUIRE((cudd_true == cudd::Bvec::bvec_true(manager, size)).IsOne());
            REQUIRE(cudd_true.bvec_val() == (val + inv));
            REQUIRE((cudd_res == cudd::Bvec::bvec_con(manager, size, con + val)).IsOne());
            REQUIRE(cudd_res.bvec_val() == (val + con));
            REQUIRE((cudd_inv_res == cudd::Bvec::bvec_con(manager, size, con + inv)).IsOne());
            REQUIRE(cudd_inv_res.bvec_val() == (inv + con));

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_small = sylvan::Bvec::bvec_con(size, con);
            sylvan::Bvec sylvan_inv = sylvan::Bvec::bvec_con(size, inv);
            sylvan::Bvec sylvan_true = bvec_sylvan + sylvan_inv;
            sylvan::Bvec sylvan_res = bvec_sylvan + sylvan_small;
            sylvan::Bvec sylvan_inv_res = sylvan_inv + sylvan_small;
            REQUIRE((sylvan_true == sylvan::Bvec::bvec_true(size)).isOne());
            REQUIRE(sylvan_true.bvec_val() == (val + inv));
            REQUIRE((sylvan_res == sylvan::Bvec::bvec_con(size, con + val)).isOne());
            REQUIRE(sylvan_res.bvec_val() == (val + con));
            REQUIRE((sylvan_inv_res == sylvan::Bvec::bvec_con(size, con + inv)).isOne());
            REQUIRE(sylvan_inv_res.bvec_val() == (inv + con));

            buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);
            buddy::bvec_ptr buddy_small = buddy::bvec_con_buddy(size, con);
            buddy::bvec_ptr buddy_inv = buddy::bvec_con_buddy(size, inv);
            buddy::bvec_ptr buddy_true = bvec_buddy + buddy_inv;
            buddy::bvec_ptr buddy_res = bvec_buddy + buddy_small;
            buddy::bvec_ptr buddy_inv_res = buddy_inv + buddy_small;
            REQUIRE((buddy_true == buddy::bvec_true_buddy(size)).is_true());
            REQUIRE(buddy_true.bvec_value() == (val + inv));
            REQUIRE((buddy_res == buddy::bvec_con_buddy(size, con + val)).is_true());
            REQUIRE(buddy_res.bvec_value() == (val + con));
            REQUIRE((buddy_inv_res == buddy::bvec_con_buddy(size, con + inv)).is_true());
            REQUIRE(buddy_inv_res.bvec_value() == (inv + con));

            REQUIRE(isEqual(cudd_true, sylvan_true, buddy_true));
            REQUIRE(isEqual(cudd_res, sylvan_res, buddy_res));
            REQUIRE(isEqual(cudd_inv_res, sylvan_inv_res, buddy_inv_res));
        }

        SECTION("Test -") {
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_small = cudd::Bvec::bvec_con(manager, size, con);
            cudd::Bvec cudd_inv = cudd::Bvec::bvec_con(manager, size, inv);
            cudd::Bvec cudd_diff = cudd_inv - bvec_cudd;
            cudd::Bvec cudd_res = bvec_cudd - cudd_small;
            cudd::Bvec cudd_inv_res = cudd_inv - cudd_small;
            REQUIRE((cudd_diff == cudd::Bvec::bvec_con(manager, size, inv - val)).IsOne());
            REQUIRE(cudd_diff.bvec_val() == (inv - val));
            REQUIRE((cudd_res == cudd::Bvec::bvec_con(manager, size, val - con)).IsOne());
            REQUIRE(cudd_res.bvec_val() == (val - con));
            REQUIRE((cudd_inv_res == cudd::Bvec::bvec_con(manager, size, inv - con)).IsOne());
            REQUIRE(cudd_inv_res.bvec_val() == (inv - con));

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_small = sylvan::Bvec::bvec_con(size, con);
            sylvan::Bvec sylvan_inv = sylvan::Bvec::bvec_con(size, inv);
            sylvan::Bvec sylvan_diff = sylvan_inv - bvec_sylvan;
            sylvan::Bvec sylvan_res = bvec_sylvan - sylvan_small;
            sylvan::Bvec sylvan_inv_res = sylvan_inv - sylvan_small;
            REQUIRE((sylvan_diff == sylvan::Bvec::bvec_con(size, inv - val)).isOne());
            REQUIRE(sylvan_diff.bvec_val() == (inv - val));
            REQUIRE((sylvan_res == sylvan::Bvec::bvec_con(size, val - con)).isOne());
            REQUIRE(sylvan_res.bvec_val() == (val - con));
            REQUIRE((sylvan_inv_res == sylvan::Bvec::bvec_con(size, inv - con)).isOne());
            REQUIRE(sylvan_inv_res.bvec_val() == (inv - con));

            buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);
            buddy::bvec_ptr buddy_small = buddy::bvec_con_buddy(size, con);
            buddy::bvec_ptr buddy_inv = buddy::bvec_con_buddy(size, inv);
            buddy::bvec_ptr buddy_diff = buddy_inv - bvec_buddy;
            buddy::bvec_ptr buddy_res = bvec_buddy - buddy_small;
            buddy::bvec_ptr buddy_inv_res = buddy_inv - buddy_small;
            REQUIRE((buddy_diff == buddy::bvec_con_buddy(size, inv - val)).is_true());
            REQUIRE(buddy_diff.bvec_value() == (inv - val));
            REQUIRE((buddy_res == buddy::bvec_con_buddy(size, val - con)).is_true());
            REQUIRE(buddy_res.bvec_value() == (val - con));
            REQUIRE((buddy_inv_res == buddy::bvec_con_buddy(size, inv - con)).is_true());
            REQUIRE(buddy_inv_res.bvec_value() == (inv - con));

            REQUIRE(isEqual(cudd_diff, sylvan_diff, buddy_diff));
            REQUIRE(isEqual(cudd_res, sylvan_res, buddy_res));
            REQUIRE(isEqual(cudd_inv_res, sylvan_inv_res, buddy_inv_res));
        }

        SECTION("Test * bitvector") {
            size = 16U;
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_small = cudd::Bvec::bvec_con(manager, size, con);
            cudd::Bvec cudd_inv = cudd::Bvec::bvec_con(manager, size, inv);
            cudd::Bvec cudd_res = bvec_cudd * cudd_small;
            cudd::Bvec cudd_inv_res = cudd_inv * cudd_small;
            REQUIRE((cudd_res == cudd::Bvec::bvec_con(manager, size + size, con * val)).IsOne());
            REQUIRE(cudd_res.bvec_val() == (val * con));
            REQUIRE((cudd_inv_res == cudd::Bvec::bvec_con(manager, size + size, con * inv)).IsOne());
            REQUIRE(cudd_inv_res.bvec_val() == (inv * con));

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_small = sylvan::Bvec::bvec_con(size, con);
            sylvan::Bvec sylvan_inv = sylvan::Bvec::bvec_con(size, inv);
            sylvan::Bvec sylvan_res = bvec_sylvan * sylvan_small;
            sylvan::Bvec sylvan_inv_res = sylvan_inv * sylvan_small;

            REQUIRE((sylvan_res == sylvan::Bvec::bvec_con(size + size, con * val)).isOne());
            REQUIRE(sylvan_res.bvec_val() == (val * con));
            REQUIRE((sylvan_inv_res == sylvan::Bvec::bvec_con(size + size, con * inv)).isOne());
            REQUIRE(sylvan_inv_res.bvec_val() == (inv * con));

            buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);
            buddy::bvec_ptr buddy_small = buddy::bvec_con_buddy(size, con);
            buddy::bvec_ptr buddy_inv = buddy::bvec_con_buddy(size, inv);
            buddy::bvec_ptr buddy_res = bvec_buddy * buddy_small;
            buddy::bvec_ptr buddy_inv_res = buddy_inv * buddy_small;
            REQUIRE((buddy_res == buddy::bvec_con_buddy(size + size, con * val)).is_true());
            REQUIRE(buddy_res.bvec_value() == (val * con));
            REQUIRE((buddy_inv_res == buddy::bvec_con_buddy(size + size, con * inv)).is_true());
            REQUIRE(buddy_inv_res.bvec_value() == (inv * con));

            REQUIRE(isEqual(cudd_res, sylvan_res, buddy_res));
            REQUIRE(isEqual(cudd_inv_res, sylvan_inv_res, buddy_inv_res));
        }

        SECTION("Test * constant") {
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_inv = cudd::Bvec::bvec_con(manager, size, inv);
            cudd::Bvec cudd_res = bvec_cudd * con;
            REQUIRE((cudd_res == cudd::Bvec::bvec_con(manager, size, con * val)).IsOne());
            REQUIRE(cudd_res.bvec_val() == (val * con));

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_inv = sylvan::Bvec::bvec_con(size, inv);
            sylvan::Bvec sylvan_res = bvec_sylvan * con;
            REQUIRE((sylvan_res == sylvan::Bvec::bvec_con(size, con * val)).isOne());
            REQUIRE(sylvan_res.bvec_val() == (val * con));

            buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);
            buddy::bvec_ptr buddy_inv = buddy::bvec_con_buddy(size, inv);
            buddy::bvec_ptr buddy_res = bvec_buddy * con;
            REQUIRE((buddy_res == buddy::bvec_con_buddy(size, con * val)).is_true());
            REQUIRE(buddy_res.bvec_value() == (val * con));

            REQUIRE(isEqual(cudd_res, sylvan_res, buddy_res));
        }

        SECTION("Test unsigned div") {
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_div = cudd::Bvec::bvec_con(manager, size, con);
            cudd::Bvec cudd_res(manager);
            cudd::Bvec cudd_rem(manager);
            cudd::Bvec::bvec_div(bvec_cudd, cudd_div, cudd_res, cudd_rem);

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_div = sylvan::Bvec::bvec_con(size, con);
            sylvan::Bvec sylvan_res{};
            sylvan::Bvec sylvan_rem{};
            sylvan::Bvec::bvec_div(bvec_sylvan, sylvan_div, sylvan_res, sylvan_rem);

            buddy::bvec_ptr bvec_buddy = buddy::bvec_con_buddy(size, val);
            buddy::bvec_ptr buddy_div = buddy::bvec_con_buddy(size, con);
            buddy::bvec_ptr buddy_res{};
            buddy::bvec_ptr buddy_rem{};
            buddy::bvec_div_buddy(bvec_buddy, buddy_div, buddy_res, buddy_rem);

            REQUIRE(isEqual(cudd_res, sylvan_res, buddy_res));
            REQUIRE(isEqual(cudd_rem, sylvan_rem, buddy_rem));
        }

        SECTION("Test signed div") {
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_div = cudd::Bvec::bvec_ncon(manager, size, -5);
            REQUIRE(cudd_div.bvec_nval() == -5);
            cudd::Bvec cudd_res(manager);
            cudd::Bvec cudd_rem(manager);
            cudd::Bvec::bvec_sdiv(bvec_cudd, cudd_div, cudd_res, cudd_rem);
            REQUIRE(cudd_res.bvec_nval() == -8);

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_div = sylvan::Bvec::bvec_ncon(size, -5);
            REQUIRE(sylvan_div.bvec_nval() == -5);
            sylvan::Bvec sylvan_res{};
            sylvan::Bvec sylvan_rem{};
            sylvan::Bvec::bvec_sdiv(bvec_sylvan, sylvan_div, sylvan_res, sylvan_rem);
            REQUIRE(sylvan_res.bvec_nval() == -8);

            REQUIRE(isEqual(cudd_res, sylvan_res));
            REQUIRE(isEqual(cudd_rem, sylvan_rem));
        }
    }

    SECTION("Testing variables") {
        uint8_t offset = 0U;
        uint8_t step = 2U;
        uint8_t con = 5U;

        SECTION("Test +") {
            cudd::Bvec cuddX = cudd::Bvec::bvec_var(manager, size, offset, step);
            cudd::Bvec cuddY = cudd::Bvec::bvec_con(manager, size, con);
            cudd::Bvec cudd_res = cuddX + cuddY;
            cudd::Bvec cudd_inv = cuddY + cuddX;
            REQUIRE((cudd_res == cudd_inv).IsOne());
            BDD cudd_expected = (cuddX == cudd::Bvec::bvec_con(manager, size, 2U));
            BDD cudd_result = (cudd_res == cudd::Bvec::bvec_con(manager, size, 7U));
            BDD cudd_miter = cudd_result.Xnor(cudd_expected);
            REQUIRE(cudd_miter.IsOne());

            sylvan::Bvec sylvanX = sylvan::Bvec::bvec_var(size, offset, step);
            sylvan::Bvec sylvanY = sylvan::Bvec::bvec_con(size, con);
            sylvan::Bvec sylvan_res = sylvanX + sylvanY;
            sylvan::Bvec sylvan_inv = sylvanY + sylvanX;
            REQUIRE((sylvan_res == sylvan_inv).isOne());
            sylvan::Bdd sylvan_expected = (sylvanX == sylvan::Bvec::bvec_con(size, 2U));
            sylvan::Bdd sylvan_result = (sylvan_res == sylvan::Bvec::bvec_con(size, 7U));
            sylvan::Bdd sylvan_miter = sylvan_result.Xnor(sylvan_expected);
            REQUIRE(sylvan_miter.isOne());

            buddy::bvec_ptr buddyX = buddy::bvec_var_buddy(size, offset, step);
            buddy::bvec_ptr buddyY = buddy::bvec_con_buddy(size, con);
            buddy::bvec_ptr buddy_res = buddyX + buddyY;
            buddy::bvec_ptr buddy_inv = buddyY + buddyX;
            REQUIRE((buddy_res == buddy_inv).is_true());
            buddy::bdd_ptr buddy_expected = (buddyX == buddy::bvec_con_buddy(size, 2U));
            buddy::bdd_ptr buddy_result = (buddy_res == buddy::bvec_con_buddy(size, 7U));
            buddy::bdd_ptr buddy_miter = !(buddy_result ^ buddy_expected);
            REQUIRE(buddy_miter.is_true());

            REQUIRE(cudd_res.bvec_val() == buddy_res.bvec_value());
            REQUIRE(cudd_inv.bvec_val() == buddy_res.bvec_value());
            REQUIRE(sylvan_res.bvec_val() == buddy_res.bvec_value());
            REQUIRE(sylvan_inv.bvec_val() == buddy_res.bvec_value());
            REQUIRE(cudd_res.bvec_isConst() == buddy_res.is_const());
            REQUIRE(cudd_inv.bvec_isConst() == buddy_inv.is_const());
            REQUIRE(sylvan_res.bvec_isConst() == buddy_res.is_const());
            REQUIRE(sylvan_inv.bvec_isConst() == buddy_inv.is_const());

            REQUIRE(isEqualVar(manager, cudd_miter, sylvan_miter, buddy_miter));
            REQUIRE(isEqualVar(cudd_res, sylvan_res, buddy_res));
            REQUIRE(isEqualVar(cudd_inv, sylvan_inv, buddy_inv));
        }

        SECTION("Test -") {
            cudd::Bvec cuddX = cudd::Bvec::bvec_var(manager, size, offset, step);
            cudd::Bvec cuddY = cudd::Bvec::bvec_con(manager, size, con);
            cudd::Bvec cudd_res = cuddX - cuddY;
            BDD cudd_expected = (cuddX == cudd::Bvec::bvec_con(manager, size, 7U));
            BDD cudd_result = (cudd_res == cudd::Bvec::bvec_con(manager, size, 2U));
            BDD cudd_miter = cudd_result.Xnor(cudd_expected);
            REQUIRE(cudd_miter.IsOne());

            sylvan::Bvec sylvanX = sylvan::Bvec::bvec_var(size, offset, step);
            sylvan::Bvec sylvanY = sylvan::Bvec::bvec_con(size, con);
            sylvan::Bvec sylvan_res = sylvanX - sylvanY;
            sylvan::Bdd sylvan_expected = (sylvanX == sylvan::Bvec::bvec_con(size, 7U));
            sylvan::Bdd sylvan_result = (sylvan_res == sylvan::Bvec::bvec_con(size, 2U));
            sylvan::Bdd sylvan_miter = sylvan_result.Xnor(sylvan_expected);
            REQUIRE(sylvan_miter.isOne());

            buddy::bvec_ptr buddyX = buddy::bvec_var_buddy(size, offset, step);
            buddy::bvec_ptr buddyY = buddy::bvec_con_buddy(size, con);
            buddy::bvec_ptr buddy_res = buddyX - buddyY;
            buddy::bdd_ptr buddy_expected = (buddyX == buddy::bvec_con_buddy(size, 7U));
            buddy::bdd_ptr buddy_result = (buddy_res == buddy::bvec_con_buddy(size, 2U));
            buddy::bdd_ptr buddy_miter = !(buddy_result ^ buddy_expected);
            REQUIRE(buddy_miter.is_true());

            REQUIRE(cudd_res.bvec_val() == buddy_res.bvec_value());
            REQUIRE(sylvan_res.bvec_val() == buddy_res.bvec_value());
            REQUIRE(cudd_res.bvec_isConst() == buddy_res.is_const());
            REQUIRE(sylvan_res.bvec_isConst() == buddy_res.is_const());

            REQUIRE(isEqualVar(manager, cudd_miter, sylvan_miter, buddy_miter));
            REQUIRE(isEqualVar(cudd_res, sylvan_res, buddy_res));
        }

        SECTION("Test * bitvector") {
            cudd::Bvec cuddX = cudd::Bvec::bvec_var(manager, size, offset, step);
            cudd::Bvec cuddY = cudd::Bvec::bvec_con(manager, size, con);
            cudd::Bvec cudd_res = (cuddY * cuddX).bvec_coerce(size);
            cudd::Bvec cudd_inv = (cuddX * cuddY).bvec_coerce(size);
            REQUIRE((cudd_res == cudd_inv).IsOne());
            BDD cudd_expected = (cuddX == cudd::Bvec::bvec_con(manager, size, 2U));
            BDD cudd_result = (cudd_res == cudd::Bvec::bvec_con(manager, size, 10U));
            BDD cudd_miter = cudd_result.Xnor(cudd_expected);
            REQUIRE(cudd_miter.IsOne());

            sylvan::Bvec sylvanX = sylvan::Bvec::bvec_var(size, offset, step);
            sylvan::Bvec sylvanY = sylvan::Bvec::bvec_con(size, con);
            sylvan::Bvec sylvan_res = (sylvanY * sylvanX).bvec_coerce(size);
            sylvan::Bvec sylvan_inv = (sylvanX * sylvanY).bvec_coerce(size);
            sylvan::Bdd sylvan_expected = (sylvanX == sylvan::Bvec::bvec_con(size, 2U));
            sylvan::Bdd sylvan_result = (sylvan_res == sylvan::Bvec::bvec_con(size, 10U));
            sylvan::Bdd sylvan_miter = sylvan_result.Xnor(sylvan_expected);
            REQUIRE(sylvan_miter.isOne());
            REQUIRE((sylvan_res == sylvan_inv).isOne());

            buddy::bvec_ptr buddyX = buddy::bvec_var_buddy(size, offset, step);
            buddy::bvec_ptr buddyY = buddy::bvec_con_buddy(size, con);
            buddy::bvec_ptr buddy_res = (buddyY * buddyX).coerce(size);
            buddy::bvec_ptr buddy_inv = (buddyX * buddyY).coerce(size);
            buddy::bdd_ptr buddy_expected = (buddyX == buddy::bvec_con_buddy(size, 2U));
            buddy::bdd_ptr buddy_result = (buddy_res == buddy::bvec_con_buddy(size, 10U));
            buddy::bdd_ptr buddy_miter = !(buddy_result ^ buddy_expected);
            REQUIRE(buddy_miter.is_true());
            REQUIRE((buddy_res == buddy_inv).is_true());

            REQUIRE(cudd_res.bvec_val() == buddy_res.bvec_value());
            REQUIRE(cudd_inv.bvec_val() == buddy_res.bvec_value());
            REQUIRE(sylvan_res.bvec_val() == buddy_res.bvec_value());
            REQUIRE(sylvan_inv.bvec_val() == buddy_res.bvec_value());
            REQUIRE(cudd_res.bvec_isConst() == buddy_res.is_const());
            REQUIRE(cudd_inv.bvec_isConst() == buddy_inv.is_const());
            REQUIRE(sylvan_res.bvec_isConst() == buddy_res.is_const());
            REQUIRE(sylvan_inv.bvec_isConst() == buddy_inv.is_const());

            REQUIRE(isEqualVar(manager, cudd_miter, sylvan_miter, buddy_miter));
            REQUIRE(isEqualVar(cudd_res, sylvan_res, buddy_res));
            REQUIRE(isEqualVar(cudd_inv, sylvan_inv, buddy_inv));
        }

        SECTION("Test * constant") {
            uint8_t con = 5U;
            cudd::Bvec cudd = cudd::Bvec::bvec_var(manager, size, offset, step);
            cudd::Bvec cudd_res = cudd * con;
            BDD cudd_expected = (cudd == cudd::Bvec::bvec_con(manager, size, 2U));
            BDD cudd_result = (cudd_res == cudd::Bvec::bvec_con(manager, size, 10U));
            BDD cudd_miter = cudd_result.Xnor(cudd_expected);
            REQUIRE(cudd_miter.IsOne());

            sylvan::Bvec sylvan = sylvan::Bvec::bvec_var(size, offset, step);
            sylvan::Bvec sylvan_res = sylvan * con;
            sylvan::Bdd sylvan_expected = (sylvan == sylvan::Bvec::bvec_con(size, 2U));
            sylvan::Bdd sylvan_result = (sylvan_res == sylvan::Bvec::bvec_con(size, 10U));
            sylvan::Bdd sylvan_miter = sylvan_result.Xnor(sylvan_expected);
            REQUIRE(sylvan_miter.isOne());

            buddy::bvec_ptr buddy = buddy::bvec_var_buddy(size, offset, step);
            buddy::bvec_ptr buddy_res = buddy * con;
            buddy::bdd_ptr buddy_expected = (buddy == buddy::bvec_con_buddy(size, 2U));
            buddy::bdd_ptr buddy_result = (buddy_res == buddy::bvec_con_buddy(size, 10U));
            buddy::bdd_ptr buddy_miter = !(buddy_result ^ buddy_expected);
            REQUIRE(buddy_miter.is_true());

            REQUIRE(cudd_res.bvec_val() == buddy_res.bvec_value());
            REQUIRE(sylvan_res.bvec_val() == buddy_res.bvec_value());
            REQUIRE(cudd_res.bvec_isConst() == buddy_res.is_const());
            REQUIRE(sylvan_res.bvec_isConst() == buddy_res.is_const());

            REQUIRE(isEqualVar(manager, cudd_miter, sylvan_miter, buddy_miter));
            REQUIRE(isEqualVar(cudd_res, sylvan_res, buddy_res));
        }

        SECTION("Test unsigned div") {
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_var(manager, size, offset, step);
            cudd::Bvec cudd_div = cudd::Bvec::bvec_con(manager, size, con);
            cudd::Bvec cudd_res(manager);
            cudd::Bvec cudd_rem(manager);
            cudd::Bvec::bvec_div(bvec_cudd, cudd_div, cudd_res, cudd_rem);
            BDD cudd_expected = (bvec_cudd >= cudd::Bvec::bvec_con(manager, size, 10U)
                & bvec_cudd < cudd::Bvec::bvec_con(manager, size, 15U));
            BDD cudd_result = (cudd_res == cudd::Bvec::bvec_con(manager, size, 2U));
            BDD cudd_miter = cudd_result.Xnor(cudd_expected);
            REQUIRE(cudd_miter.IsOne());

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_var(size, offset, step);
            sylvan::Bvec sylvan_div = sylvan::Bvec::bvec_con(size, con);
            sylvan::Bvec sylvan_res{};
            sylvan::Bvec sylvan_rem{};
            sylvan::Bvec::bvec_div(bvec_sylvan, sylvan_div, sylvan_res, sylvan_rem);
            sylvan::Bdd sylvan_expected = (bvec_sylvan >= sylvan::Bvec::bvec_con(size, 10U)
                & bvec_sylvan < sylvan::Bvec::bvec_con(size, 15U));
            sylvan::Bdd sylvan_result = (sylvan_res == sylvan::Bvec::bvec_con(size, 2U));
            sylvan::Bdd sylvan_miter = sylvan_result.Xnor(sylvan_expected);
            REQUIRE(sylvan_miter.isOne());

            buddy::bvec_ptr bvec_buddy = buddy::bvec_var_buddy(size, offset, step);
            buddy::bvec_ptr buddy_div = buddy::bvec_con_buddy(size, con);
            buddy::bvec_ptr buddy_res{};
            buddy::bvec_ptr buddy_rem{};
            buddy::bvec_div_buddy(bvec_buddy, buddy_div, buddy_res, buddy_rem);
            buddy::bdd_ptr buddy_expected = (bvec_buddy >= buddy::bvec_con_buddy(size, 10U)
                & bvec_buddy < buddy::bvec_con_buddy(size, 15U));
            buddy::bdd_ptr buddy_result = (buddy_res == buddy::bvec_con_buddy(size, 2U));
            buddy::bdd_ptr buddy_miter = !(buddy_result ^ buddy_expected);
            REQUIRE(buddy_miter.is_true());

            REQUIRE(cudd_res.bvec_val() == buddy_res.bvec_value());
            REQUIRE(cudd_rem.bvec_val() == buddy_rem.bvec_value());
            REQUIRE(sylvan_res.bvec_val() == buddy_res.bvec_value());
            REQUIRE(sylvan_rem.bvec_val() == buddy_rem.bvec_value());
            REQUIRE(cudd_res.bvec_isConst() == buddy_res.is_const());
            REQUIRE(cudd_rem.bvec_isConst() == buddy_rem.is_const());
            REQUIRE(sylvan_res.bvec_isConst() == buddy_res.is_const());
            REQUIRE(sylvan_rem.bvec_isConst() == buddy_rem.is_const());

            REQUIRE(isEqualVar(manager, cudd_miter, sylvan_miter, buddy_miter));
            REQUIRE(isEqualVar(cudd_res, sylvan_res, buddy_res));
            REQUIRE(isEqualVar(cudd_rem, sylvan_rem, buddy_rem));
        }

        /*SECTION("Test signed div") { //fix
            cudd::Bvec bvec_cudd = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_div = cudd::Bvec::bvec_con(manager, size, -5);
            REQUIRE(cudd_div.bvec_val() == -5);
            cudd::Bvec cudd_res(manager);
            cudd::Bvec cudd_rem(manager);
            Bvec::bvec_sdiv(bvec_cudd, cudd_div, cudd_res, cudd_rem);

            sylvan::Bvec bvec_sylvan = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_div = sylvan::Bvec::bvec_con(size, -5);
            REQUIRE(sylvan_div.bvec_val() == -5);
            sylvan::Bvec sylvan_res{};
            sylvan::Bvec sylvan_rem{};
            sylvan::Bvec::bvec_sdiv(bvec_sylvan, sylvan_div, sylvan_res, sylvan_rem);

            REQUIRE(isEqual(cudd_res, sylvan_res)).get_bvec();
            REQUIRE(isEqual(cudd_rem, sylvan_rem)).get_bvec();
        }*/
    }

    finish();
}

TEST_CASE("Test compare operators") {
    init();
    Cudd manager;
    size_t size = 0U;

    SECTION("Testing constants") {
        uint8_t val = 42U;
        uint8_t inv = ~val;
        size = 8U;

        SECTION("Testing unsigned lth, lte operators") {
            cudd::Bvec cudd_small = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_big = cudd::Bvec::bvec_con(manager, size, inv);
            BDD res_cudd = cudd_small < cudd_big;
            REQUIRE(res_cudd.IsOne());
            res_cudd = cudd_small <= cudd_big;
            REQUIRE(res_cudd.IsOne());
            res_cudd = cudd_big <= cudd_small;
            REQUIRE(!res_cudd.IsOne());
            res_cudd = cudd_big < cudd_small;
            REQUIRE(!(res_cudd.IsOne()));
            res_cudd = cudd_small > cudd_small;
            REQUIRE(!(res_cudd.IsOne()));
            res_cudd = cudd_small >= cudd_small;
            REQUIRE(res_cudd.IsOne());

            sylvan::Bvec sylvan_small = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_big = sylvan::Bvec::bvec_con(size, inv);
            sylvan::Bdd res_sylvan = sylvan_small < sylvan_big;
            REQUIRE(res_sylvan.isOne());
            res_sylvan = sylvan_small <= sylvan_big;
            REQUIRE(res_sylvan.isOne());
            res_sylvan = sylvan_big <= sylvan_small;
            REQUIRE(!res_sylvan.isOne());
            res_sylvan = sylvan_big < sylvan_small;
            REQUIRE(!(res_sylvan.isOne()));
            res_sylvan = sylvan_small > sylvan_small;
            REQUIRE(!(res_sylvan.isOne()));
            res_sylvan = sylvan_small >= sylvan_small;
            REQUIRE(res_sylvan.isOne());

            buddy::bvec_ptr buddy_small = buddy::bvec_con_buddy(size, val);
            buddy::bvec_ptr buddy_big = buddy::bvec_con_buddy(size, inv);
            buddy::bdd_ptr res_buddy = buddy_small < buddy_big;
            REQUIRE(res_buddy.is_true());
            res_buddy = buddy_small <= buddy_big;
            REQUIRE(res_buddy.is_true());
            res_buddy = buddy_big <= buddy_small;
            REQUIRE(res_buddy.is_false());
            res_buddy = buddy_big < buddy_small;
            REQUIRE(res_buddy.is_false());
            res_buddy = buddy_small > buddy_small;
            REQUIRE(res_buddy.is_false());
            res_buddy = buddy_small >= buddy_small;
            REQUIRE(res_buddy.is_true());
        }

        SECTION("Testing signed lth, lte operators") {
            cudd::Bvec cudd_small = cudd::Bvec::bvec_ncon(manager, size, -84);
            cudd::Bvec cudd_big = cudd::Bvec::bvec_ncon(manager, size, -42);
            BDD res_cudd = cudd_small < cudd_big;
            REQUIRE(res_cudd.IsOne());
            res_cudd = cudd_small <= cudd_big;
            REQUIRE(res_cudd.IsOne());
            res_cudd = cudd_big <= cudd_small;
            REQUIRE(!res_cudd.IsOne());
            res_cudd = cudd_big < cudd_small;
            REQUIRE(!res_cudd.IsOne());
            res_cudd = cudd_small > cudd_small;
            REQUIRE(!res_cudd.IsOne());
            res_cudd = cudd_small >= cudd_small;
            REQUIRE(res_cudd.IsOne());

            sylvan::Bvec sylvan_small = sylvan::Bvec::bvec_ncon(size, -84);
            sylvan::Bvec sylvan_big = sylvan::Bvec::bvec_ncon(size, -42);
            sylvan::Bdd res_sylvan = sylvan_small < sylvan_big;
            REQUIRE(res_sylvan.isOne());
            res_sylvan = sylvan_small <= sylvan_big;
            REQUIRE(res_sylvan.isOne());
            res_sylvan = sylvan_big <= sylvan_small;
            REQUIRE(!res_sylvan.isOne());
            res_sylvan = sylvan_big < sylvan_small;
            REQUIRE(!res_sylvan.isOne());
            res_sylvan = sylvan_small > sylvan_small;
            REQUIRE(!res_sylvan.isOne());
            res_sylvan = sylvan_small >= sylvan_small;
            REQUIRE(res_sylvan.isOne());
        }

        SECTION("Testing equality operator") {
            cudd::Bvec cudd_small = cudd::Bvec::bvec_con(manager, size, val);
            cudd::Bvec cudd_big = cudd::Bvec::bvec_con(manager, size, inv);
            BDD res_cudd = cudd_small == cudd_big;
            REQUIRE(!res_cudd.IsOne());
            res_cudd = cudd_small != cudd_big;
            REQUIRE(res_cudd.IsOne());
            res_cudd = cudd_big == cudd_big;
            REQUIRE(res_cudd.IsOne());
            res_cudd = cudd_small == cudd_small;
            REQUIRE(res_cudd.IsOne());
            res_cudd = cudd_small != cudd_small;
            REQUIRE(!res_cudd.IsOne());
            res_cudd = cudd_big != cudd_big;
            REQUIRE(!res_cudd.IsOne());

            sylvan::Bvec sylvan_small = sylvan::Bvec::bvec_con(size, val);
            sylvan::Bvec sylvan_big = sylvan::Bvec::bvec_con(size, inv);
            sylvan::Bdd res_sylvan = sylvan_small == sylvan_big;
            REQUIRE(!(res_sylvan.isOne()));
            res_sylvan = sylvan_small != sylvan_big;
            REQUIRE(res_sylvan.isOne());
            res_sylvan = sylvan_big == sylvan_big;
            REQUIRE(res_sylvan.isOne());
            res_sylvan = sylvan_small == sylvan_small;
            REQUIRE(res_sylvan.isOne());
            res_sylvan = sylvan_small != sylvan_small;
            REQUIRE(!(res_sylvan.isOne()));
            res_sylvan = sylvan_big != sylvan_big;
            REQUIRE(!(res_sylvan.isOne()));

            buddy::bvec_ptr buddy_small = buddy::bvec_con_buddy(size, val);
            buddy::bvec_ptr buddy_big = buddy::bvec_con_buddy(size, inv);
            buddy::bdd_ptr res_buddy = buddy_small == buddy_big;
            REQUIRE(res_buddy.is_false());
            res_buddy = buddy_small != buddy_big;
            REQUIRE(res_buddy.is_true());
            res_buddy = buddy_big == buddy_big;
            REQUIRE(res_buddy.is_true());
            res_buddy = buddy_small == buddy_small;
            REQUIRE(res_buddy.is_true());
            res_buddy = buddy_small != buddy_small;
            REQUIRE(res_buddy.is_false());
            res_buddy = buddy_big != buddy_big;
            REQUIRE(res_buddy.is_false());
        }

    }

    SECTION("Variables") {
        uint8_t offset = 0U;
        uint8_t step = 2U;
        uint8_t con = 5U;

        SECTION("Testing unsigned lth, lte operators") {
            cudd::Bvec cuddX = cudd::Bvec::bvec_var(manager, size, offset, step);
            cudd::Bvec cuddY = cudd::Bvec::bvec_con(manager, size, con);
            BDD cudd_expected = (cuddX < cudd::Bvec::bvec_con(manager, size, con));
            BDD res_cudd = cuddX < cuddY;
            BDD cudd_miter = res_cudd.Xnor(cudd_expected);
            REQUIRE(cudd_miter.IsOne());

            res_cudd = cuddX <= cuddY;
            cudd_expected = (cuddX <= cuddY);
            cudd_miter = res_cudd.Xnor(cudd_expected);
            REQUIRE(cudd_miter.IsOne());

            res_cudd = cuddX > cuddY;
            cudd_expected = (cuddX > cuddY);
            cudd_miter = res_cudd.Xnor(cudd_expected);
            REQUIRE(cudd_miter.IsOne());

            res_cudd = cuddX >= cuddY;
            cudd_expected = (cuddX >= cuddY);
            cudd_miter = res_cudd.Xnor(cudd_expected);
            REQUIRE(cudd_miter.IsOne());


            sylvan::Bvec sylvanX = sylvan::Bvec::bvec_var(size, offset, step);
            sylvan::Bvec sylvanY = sylvan::Bvec::bvec_con(size, con);
            sylvan::Bdd sylvan_expected = (sylvanX < sylvanY);
            sylvan::Bdd res_sylvan = sylvanX < sylvanY;
            sylvan::Bdd sylvan_miter = res_sylvan.Xnor(sylvan_expected);
            REQUIRE(sylvan_miter.isOne());

            res_sylvan = sylvanX <= sylvanY;
            sylvan_expected = (sylvanX <= sylvanY);
            sylvan_miter = res_sylvan.Xnor(sylvan_expected);
            REQUIRE(sylvan_miter.isOne());

            res_sylvan = sylvanX > sylvanY;
            sylvan_expected = (sylvanX > sylvanY);
            sylvan_miter = res_sylvan.Xnor(sylvan_expected);
            REQUIRE(sylvan_miter.isOne());

            res_sylvan = sylvanX >= sylvanY;
            sylvan_expected = (sylvanX >= sylvanY);
            sylvan_miter = res_sylvan.Xnor(sylvan_expected);
            REQUIRE(sylvan_miter.isOne());

            buddy::bvec_ptr buddyX = buddy::bvec_var_buddy(size, offset, step);
            buddy::bvec_ptr buddyY = buddy::bvec_con_buddy(size, con);
            buddy::bdd_ptr buddy_expected = (buddyX < buddy::bvec_con_buddy(size, con));
            buddy::bdd_ptr res_buddy = buddyX < buddyY;
            buddy::bdd_ptr buddy_miter = !(res_buddy ^ buddy_expected);
            REQUIRE(buddy_miter.is_true());

            res_buddy = buddyX <= buddyY;
            buddy_expected = (buddyX <= buddy::bvec_con_buddy(size, con));
            buddy_miter = !(res_buddy ^ buddy_expected);
            REQUIRE(buddy_miter.is_true());

            res_buddy = buddyX > buddyY;
            buddy_expected = (buddyX > buddy::bvec_con_buddy(size, con));
            buddy_miter = !(res_buddy ^ buddy_expected);
            REQUIRE(buddy_miter.is_true());

            res_buddy = buddyX >= buddyY;
            buddy_expected = (buddyX >= buddy::bvec_con_buddy(size, con));
            buddy_miter = !(res_buddy ^ buddy_expected);
            REQUIRE(buddy_miter.is_true());

            REQUIRE(isEqualVar(manager, cudd_miter, sylvan_miter, buddy_miter));
            REQUIRE(isEqualVar(manager, res_cudd, res_sylvan, res_buddy));
        }

        /*SECTION("Testing signed lth, lte operators") {
            cudd::Bvec cudd_small = cudd::Bvec::bvec_con(manager, size, -84);
            cudd::Bvec cudd_big = cudd::Bvec::bvec_con(manager, size, -42);
            BDD res_cudd = cudd_small < cudd_big;
            REQUIRE(res_cudd.IsOne());
            res_cudd = cudd_small <= cudd_big;
            REQUIRE(res_cudd.IsOne());
            res_cudd = cudd_big <= cudd_small;
            REQUIRE(!res_cudd.IsOne());
            res_cudd = cudd_big < cudd_small;
            REQUIRE(!res_cudd.IsOne());
            res_cudd = cudd_small > cudd_small;
            REQUIRE(!res_cudd.IsOne());
            res_cudd = cudd_small >= cudd_small;
            REQUIRE(res_cudd.IsOne());

            sylvan::Bvec sylvan_small = sylvan::Bvec::bvec_con(size, -84);
            sylvan::Bvec sylvan_big = sylvan::Bvec::bvec_con(size, -42);
            sylvan::Bdd res_sylvan = sylvan_small < sylvan_big;
            REQUIRE(res_sylvan.isOne());
            res_sylvan = sylvan_small <= sylvan_big;
            REQUIRE(res_sylvan.isOne());
            res_sylvan = sylvan_big <= sylvan_small;
            REQUIRE(!res_sylvan.isOne());
            res_sylvan = sylvan_big < sylvan_small;
            REQUIRE(!res_sylvan.isOne());
            res_sylvan = sylvan_small > sylvan_small;
            REQUIRE(!res_sylvan.isOne());
            res_sylvan = sylvan_small >= sylvan_small;
            REQUIRE(res_sylvan.isOne());
        }*/

        SECTION("Testing equality operator") {
            cudd::Bvec cuddX = cudd::Bvec::bvec_var(manager, size, offset, step);
            cudd::Bvec cuddY = cudd::Bvec::bvec_con(manager, size, con);
            BDD cudd_expected = (cuddX == cuddY);
            BDD res_cudd = cuddX == cuddY;
            BDD cudd_inv = cuddY == cuddX;
            REQUIRE(res_cudd == cudd_inv);
            BDD cudd_miter = res_cudd.Xnor(cudd_expected);
            REQUIRE(cudd_miter.IsOne());

            cudd_expected = (cuddX != cuddY);
            res_cudd = cuddX != cuddY;
            cudd_inv = cuddY != cuddX;
            REQUIRE(res_cudd == cudd_inv);
            cudd_miter = res_cudd.Xnor(cudd_expected);
            REQUIRE(cudd_miter.IsOne());


            sylvan::Bvec sylvanX = sylvan::Bvec::bvec_var(size, offset, step);
            sylvan::Bvec sylvanY = sylvan::Bvec::bvec_con(size, con);
            sylvan::Bdd sylvan_expected = (sylvanX == sylvanY);
            sylvan::Bdd res_sylvan = sylvanX == sylvanY;
            sylvan::Bdd sylvan_inv = sylvanY == sylvanX;
            REQUIRE(res_sylvan == sylvan_inv);
            sylvan::Bdd sylvan_miter = res_sylvan.Xnor(sylvan_expected);
            REQUIRE(sylvan_miter.isOne());

            sylvan_expected = (sylvanX != sylvanY);
            res_sylvan = sylvanX != sylvanY;
            sylvan_inv = sylvanY != sylvanX;
            REQUIRE(res_sylvan == sylvan_inv);
            sylvan_miter = res_sylvan.Xnor(sylvan_expected);
            REQUIRE(sylvan_miter.isOne());

            buddy::bvec_ptr buddyX = buddy::bvec_var_buddy(size, offset, step);
            buddy::bvec_ptr buddyY = buddy::bvec_con_buddy(size, con);
            buddy::bdd_ptr buddy_expected = (buddyX == buddyY);
            buddy::bdd_ptr res_buddy = buddyX == buddyY;
            buddy::bdd_ptr buddy_inv = buddyY == buddyX;
            REQUIRE(res_buddy == buddy_inv);
            buddy::bdd_ptr buddy_miter = !(res_buddy ^ buddy_expected);
            REQUIRE(buddy_miter.is_true());

            buddy_expected = (buddyX != buddyY);
            res_buddy = buddyX != buddyY;
            buddy_inv = buddyY != buddyX;
            REQUIRE(res_buddy == buddy_inv);
            buddy_miter = !(res_buddy ^ buddy_expected);
            REQUIRE(buddy_miter.is_true());

            REQUIRE(isEqualVar(manager, cudd_miter, sylvan_miter, buddy_miter));
            REQUIRE(isEqualVar(manager, res_cudd, res_sylvan, res_buddy));
        }
    }

    finish();
}
