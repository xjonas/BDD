#include <sylvan.h>
#include <functional>
#include <iostream>

#include "../cudd/bvec_cudd.h"
#include "../sylvan/bvec_sylvan.h"
#include "../buddy/buddy_wrapper.hpp"

namespace helpers {

    void
    initSylvan();

    void
    closeSylvan();

    void
    init();

    void
    finish();

    bool
    isEqual(const cudd::Bvec& cudd, const sylvan::Bvec& sylvan, const buddy::bvec_ptr& buddy, size_t val);

    bool
    isEqual(const cudd::Bvec& cudd, const sylvan::Bvec& sylvan, const buddy::bvec_ptr& buddy);

    bool
    isEqual(const cudd::Bvec& cudd, const sylvan::Bvec& sylvan);

    BDD
    getCudd(const Cudd& manager, const buddy::bdd_ptr& buddy);

    sylvan::Bdd
    getSylvan(const buddy::bdd_ptr& buddy);

    bool
    isEqualVar(const Cudd& manager, const BDD& cudd,
        const sylvan::Bdd& sylvan, const buddy::bdd_ptr& buddy);

    bool
    isEqualVar(const cudd::Bvec& cudd, const sylvan::Bvec& sylvan, const buddy::bvec_ptr& buddy);

} // namespace
