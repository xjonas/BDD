#include "helpers.h"
#include "../buddy/buddy_wrapper.hpp"

namespace helpers {

    void
    initSylvan()
    {
        using namespace sylvan;
        int n_workers = 0; //auto-detect
        lace_init(n_workers, 0);
        lace_startup(0, NULL, NULL);
        sylvan_set_limits(512*1024*1024, 1, 5);
        sylvan_init_package();
        sylvan_init_mtbdd();
    }

    void
    closeSylvan()
    {
        using namespace sylvan;
        sylvan_stats_report(stdout);
        sylvan_quit();
        lace_exit();
    }

    void
    init()
    {
        initSylvan();
        buddy::initBuddy();

    }

    void
    finish()
    {
        closeSylvan();
        buddy::closeBuddy();
    }

    bool
    isEqual(const cudd::Bvec& cudd, const sylvan::Bvec& sylvan, const buddy::bvec_ptr& buddy, size_t val)
    {
        const Cudd& manager = cudd.manager();
        size_t size = cudd.bitnum();
        if (size != sylvan.bitnum() || size != buddy::bitnum(buddy.get_bvec())) {
            return false;
        }
        for (size_t i = 0U; i < size; ++i) {
            if (val & 1U) {
                if (manager.bddOne() != cudd[i] ||
                    !buddy[i].is_true() ||
                    sylvan::Bdd::bddOne() != sylvan[i]) {
                    return false;
                }
            } else {
                if (manager.bddZero() != cudd[i] ||
                    !buddy[i].is_false() ||
                    sylvan::Bdd::bddZero() != sylvan[i]) {
                    return false;
                }
            }
            val >>= 1U;
        }
        return true;
    }

    bool
    isEqual(const cudd::Bvec& cudd, const sylvan::Bvec& sylvan, const buddy::bvec_ptr& buddy)
    {
        const Cudd& manager = cudd.manager();
        size_t size = cudd.bitnum();
        if (size != sylvan.bitnum() || size != buddy::bitnum(buddy.get_bvec())) {
            return false;
        }
        for (size_t i = 0U; i < size; ++i) {
            if (manager.bddOne() == cudd[i]) {
                if (!buddy[i].is_true() ||
                    sylvan::Bdd::bddOne() != sylvan[i]) {
                    return false;
                }
            } else {
                if (!buddy[i].is_false() ||
                    sylvan::Bdd::bddZero() != sylvan[i]) {

                    return false;
                }
            }
        }
        return true;
    }

    bool
    isEqual(const cudd::Bvec& cudd, const sylvan::Bvec& sylvan)
    {
        const Cudd& manager = cudd.manager();
        if (cudd.bitnum() != sylvan.bitnum()) {
            return false;
        }
        for (size_t i = 0U; i < cudd.bitnum(); ++i) {
            if (manager.bddOne() == cudd[i]) {
                if (sylvan::Bdd::bddOne() != sylvan[i]) {
                    return false;
                }
            } else {
                if (sylvan::Bdd::bddZero() != sylvan[i]) {
                    return false;
                }
            }
        }
        return true;
    }

    sylvan::Bdd
    getSylvan(const buddy::bdd_ptr& buddy) {
        if (buddy.is_true()) {
            return sylvan::Bdd::bddOne();
        }
        if (buddy.is_false()) {
            return sylvan::Bdd::bddZero();
        }
        sylvan::Bdd res = sylvan::Bdd::bddVar(buddy.var());
        sylvan::Bdd low = getSylvan(buddy.low());
        sylvan::Bdd high = getSylvan(buddy.high());
        return ((low & (!res)) | (high & res));
    }

    BDD
    getCudd(const Cudd& manager, const buddy::bdd_ptr& buddy) {
        if (buddy.is_true()) {
            return manager.bddOne();
        }
        if (buddy.is_false()) {
            return manager.bddZero();
        }
        BDD res = manager.bddVar(buddy.var());
        BDD low = getCudd(manager, buddy.low());
        BDD high = getCudd(manager, buddy.high());
        return ((low & (!res)) | (high & res));
    }

    bool
    isEqualVar(const Cudd& manager, const BDD& cudd,
        const sylvan::Bdd& sylvan, const buddy::bdd_ptr& buddy) {
        BDD buddy_cudd = getCudd(manager, buddy);
        if (buddy_cudd != cudd) {
            return false;
        }
        sylvan::Bdd buddy_sylvan = getSylvan(buddy);
        if (buddy_sylvan != sylvan) {
            return false;
        }
        return true;
    }

    bool
    isEqualVar(const cudd::Bvec& cudd, const sylvan::Bvec& sylvan, const buddy::bvec_ptr& buddy) {
        if (cudd.bitnum() != sylvan.bitnum() && cudd.bitnum() != buddy::bitnum(buddy.get_bvec())) {
            return false;
        }
        for (size_t i = 0U; i < cudd.bitnum(); ++i) {
            if (!isEqualVar(cudd.manager(), cudd[i], sylvan[i], buddy[i])) {
                return false;
            }
        }
        return true;
    }

} // namespace
