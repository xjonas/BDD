#include "catch.hpp"
#include "helpers.h"
#include "../abstractBvec/managers.h"
#include "../abstractBvec/templated_bvec.hpp"

TEST_CASE("Template Cudd tests") {
    using namespace abstractBvec;
    Cudd manager;
    CuddManager cudd_manager(manager);
    using CuddManagerT = decltype(cudd_manager);

    size_t size = 8U;
    uint8_t val = 42U;
    uint8_t con = 5U;
    uint8_t inv = ~val;
    uint8_t offset = 0U;
    uint8_t step = 2U;

    SECTION("Construction tests") {

        SECTION("Test false bitvector") {
            Bvec<CuddManagerT> bvec_cudd = Bvec<CuddManagerT>::bvec_false(size, cudd_manager);
        }


        SECTION("Test true bitvector") {
            Bvec<CuddManagerT> bvec_cudd = Bvec<CuddManagerT>::bvec_true(size, cudd_manager);
        }

        SECTION("Test constant bitvector") {
            Bvec<CuddManagerT> bvec_cudd = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
        }

        SECTION("Test copy constructors") {
            Bvec<CuddManagerT> bvec_cudd = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> cudd_copy (bvec_cudd);
            Bvec<CuddManagerT> cudd_operator = bvec_cudd;
            REQUIRE((bvec_cudd == cudd_copy).IsOne());
            REQUIRE((bvec_cudd == cudd_operator).IsOne());
        }

        SECTION("Test bvec_var") {
            int offset = 1;
            int step = 2;
            Bvec<CuddManagerT> bvec_cudd = Bvec<CuddManagerT>::bvec_var(size, offset, step, cudd_manager);
        }

        SECTION("Test bvec_varvec") {
            std::vector<int> var {0, 1, 2, 3, 4, 5, 6, 7};
            Bvec<CuddManagerT> bvec_cudd = Bvec<CuddManagerT>::bvec_varvec(size, var.data(), cudd_manager);
        }
    }

    SECTION("Logic operators") {

        SECTION("Test cudd operation &/and") {
            Bvec<CuddManagerT> x = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> y = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> res = x & y;
            Bvec<CuddManagerT> res_inv = y & x;
            REQUIRE((res == y).IsOne());
            REQUIRE((res == x).IsOne());
            REQUIRE((res == res_inv).IsOne());
        }

        SECTION("Test cudd operation |/or") {
            Bvec<CuddManagerT> x = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> y = Bvec<CuddManagerT>::bvec_con(size, inv, cudd_manager);
            Bvec<CuddManagerT> res = x | y;
            Bvec<CuddManagerT> res_inv = y | x;
            REQUIRE((res == Bvec<CuddManagerT>::bvec_true(size, cudd_manager)).IsOne());
            REQUIRE((y != x).IsOne());
            REQUIRE((res_inv == Bvec<CuddManagerT>::bvec_true(size, cudd_manager)).IsOne());
        }

        SECTION("Test cudd operation ^/xor") {
            Bvec<CuddManagerT> x = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> y = Bvec<CuddManagerT>::bvec_con(size, inv, cudd_manager);
            Bvec<CuddManagerT> res = x ^ y;
            Bvec<CuddManagerT> res_inv = y ^ x;
            Bvec<CuddManagerT> res_false = x ^ x;
            REQUIRE((res == Bvec<CuddManagerT>::bvec_true(size, cudd_manager)).IsOne());
            REQUIRE((y != x).IsOne());
            REQUIRE((res_inv == Bvec<CuddManagerT>::bvec_true(size, cudd_manager)).IsOne());
            REQUIRE((res_false == Bvec<CuddManagerT>::bvec_false(size, cudd_manager)).IsOne());
        }

        SECTION("Test cudd operation !/not") {
            Bvec<CuddManagerT> x = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> y = Bvec<CuddManagerT>::bvec_con(size, inv, cudd_manager);
            Bvec<CuddManagerT> res = !x;
            Bvec<CuddManagerT> res_inv = !y;
            REQUIRE((res == y).IsOne());
            REQUIRE((res_inv == x).IsOne());
            REQUIRE((y != x).IsOne());
            res = !res;
            res_inv = !res_inv;
            REQUIRE((res == x).IsOne());
            REQUIRE((res_inv == y).IsOne());
        }

        SECTION("Test left/right shift operations") { //fix
            val = 44U;
            uint8_t bitshift = 2U;
            uint8_t l_shifted_val = val << bitshift;
            uint8_t r_shifted_val = val >> bitshift;
            Bvec<CuddManagerT> bvec = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> l_shifted = Bvec<CuddManagerT>::bvec_con(size, l_shifted_val, cudd_manager);
            Bvec<CuddManagerT> r_shifted = Bvec<CuddManagerT>::bvec_con(size, r_shifted_val, cudd_manager);
            Bvec<CuddManagerT> shifting = Bvec<CuddManagerT>::bvec_con(size, bitshift, cudd_manager);
            Bvec<CuddManagerT> res = bvec << shifting;
            REQUIRE((res == l_shifted).IsOne());
            REQUIRE(res.bvec_val() == l_shifted_val);
            //res = res >> shifting;
            //REQUIRE((res == bvec).IsOne());
            //REQUIRE(res.bvec_val() == val);

            Bvec<CuddManagerT> fixed_res = bvec << bitshift;
            REQUIRE((fixed_res == l_shifted).IsOne());
            REQUIRE(fixed_res.bvec_val() == l_shifted_val);
            fixed_res = fixed_res >> bitshift;
            REQUIRE((fixed_res == bvec).IsOne());
            REQUIRE(fixed_res.bvec_val() == val);

            //res = bvec >> shifting;
            //REQUIRE((res == r_shifted).IsOne());
            //REQUIRE(res.bvec_val() == r_shifted_val);
            //res = res << shifting;
            //REQUIRE((res == bvec).IsOne());
            //REQUIRE(res.bvec_val() == val);

            fixed_res = bvec >> bitshift;
            REQUIRE((fixed_res == r_shifted).IsOne());
            REQUIRE(fixed_res.bvec_val() == r_shifted_val);
            fixed_res = fixed_res << bitshift;
            REQUIRE(fixed_res.bvec_val() == val);
            REQUIRE((fixed_res == bvec).IsOne());
        }
    }

    SECTION("Tests arithmetic operators") {

        SECTION("Test +") {
            Bvec<CuddManagerT> bvec = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> small = Bvec<CuddManagerT>::bvec_con(size, con, cudd_manager);
            Bvec<CuddManagerT> res = bvec + small;
            Bvec<CuddManagerT> res_inv = small + bvec;
            REQUIRE((res == res_inv).IsOne());
            REQUIRE(res.bvec_val() == res_inv.bvec_val());
            REQUIRE(res.bvec_val() == (val + con));
            REQUIRE((res == Bvec<CuddManagerT>::bvec_con(size, val + con, cudd_manager)).IsOne());
        }

        SECTION("Test -") {
            Bvec<CuddManagerT> bvec = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> small = Bvec<CuddManagerT>::bvec_con(size, con, cudd_manager);
            Bvec<CuddManagerT> res = bvec - small;
            Bvec<CuddManagerT> added = res + small;
            REQUIRE(res.bvec_val() == (val - con));
            REQUIRE((bvec == added).IsOne());
            REQUIRE((small == (added - res)).IsOne());
            REQUIRE((res == Bvec<CuddManagerT>::bvec_con(size, val - con, cudd_manager)).IsOne());
        }

        SECTION("Test * bitvector") {
            Bvec<CuddManagerT> bvec = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> small = Bvec<CuddManagerT>::bvec_con(size, con, cudd_manager);
            Bvec<CuddManagerT> res = bvec * small;
            Bvec<CuddManagerT> res_inv = small * bvec;
            REQUIRE((res == res_inv).IsOne());
            REQUIRE(res.bvec_val() == res_inv.bvec_val());
            REQUIRE(res.bvec_val() == (val * con));
            REQUIRE((res == Bvec<CuddManagerT>::bvec_con(size + size, val * con, cudd_manager)).IsOne());
        }

        SECTION("Test * constant") {
            Bvec<CuddManagerT> bvec = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> res = bvec * con;
            // Bvec<CuddManagerT> res_inv = con * bvec; not possible
            // REQUIRE((res == res_inv).IsOne());
            // REQUIRE(res.bvec_val() == res_inv.bvec_val());
            REQUIRE(res.bvec_val() == (val * con));
            REQUIRE((res == Bvec<CuddManagerT>::bvec_con(size, val * con, cudd_manager)).IsOne());

        }

        SECTION("Test unsigned div") {
            Bvec<CuddManagerT> bvec = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> div = Bvec<CuddManagerT>::bvec_con(size, con, cudd_manager);
            Bvec<CuddManagerT> res(manager);
            Bvec<CuddManagerT> rem(manager);
            Bvec<CuddManagerT>::bvec_div(bvec, div, res, rem);
            REQUIRE(res.bvec_val() == (val / con));
            REQUIRE((res == Bvec<CuddManagerT>::bvec_con(size, val / con, cudd_manager)).IsOne());
            REQUIRE(rem.bvec_val() == (val % con));
            REQUIRE((rem == Bvec<CuddManagerT>::bvec_con(size, val % con, cudd_manager)).IsOne());
        }

        /*SECTION("Test signed div") { //fix
            Bvec<CuddManagerT> bvec_cudd = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> cudd_div = Bvec<CuddManagerT>::bvec_con(size, -5, cudd_manager);
            Bvec<CuddManagerT> cudd_res(manager);
            Bvec<CuddManagerT> cudd_rem(manager);
            Bvec<CuddManagerT>::bvec_sdiv(bvec_cudd, cudd_div, cudd_res, cudd_rem);
        }*/
    }

    SECTION("Test compare operators") {

        SECTION("Testing unsigned lth, lte operators") {
            Bvec<CuddManagerT> small = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> big = Bvec<CuddManagerT>::bvec_con(size, inv, cudd_manager);
            REQUIRE((small < big).IsOne());
            REQUIRE(!(big < small).IsOne());
            REQUIRE((small <= big).IsOne());
            REQUIRE(!(big <= small).IsOne());
            REQUIRE((small <= small).IsOne());
            REQUIRE(!(small < small).IsOne());
        }

        SECTION("Testing unsigned gth, gte operators") {
            Bvec<CuddManagerT> small = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> big = Bvec<CuddManagerT>::bvec_con(size, inv, cudd_manager);
            REQUIRE(!(small > big).IsOne());
            REQUIRE((big > small).IsOne());
            REQUIRE(!(small >= big).IsOne());
            REQUIRE((big >= small).IsOne());
            REQUIRE((small >= small).IsOne());
            REQUIRE(!(small > small).IsOne());
        }

        /*SECTION("Testing signed lth, lte operators") {
            Bvec<CuddManagerT> small = Bvec<CuddManagerT>::bvec_con(size, -84, cudd_manager);
            Bvec<CuddManagerT> big = Bvec<CuddManagerT>::bvec_con(size, -42, cudd_manager);
            BDD res = small < big;
            REQUIRE(res.IsOne());
            res = small <= big;
            REQUIRE(res.IsOne());
            res = big <= small;
            REQUIRE(!res.IsOne());
            res = big < small;
            REQUIRE(!res.IsOne());
            res = small > small;
            REQUIRE(!res.IsOne());
            res = small >= small;
            REQUIRE(res.IsOne());
        }

        SECTION("Testing signed gth, gte operators") {
            Bvec<CuddManagerT> small = Bvec<CuddManagerT>::bvec_con(size, -84, cudd_manager);
            Bvec<CuddManagerT> big = Bvec<CuddManagerT>::bvec_con(size, -42, cudd_manager);
            BDD res = small < big;
            REQUIRE(res.IsOne());
            res = small <= big;
            REQUIRE(res.IsOne());
            res = big <= small;
            REQUIRE(!res.IsOne());
            res = big < small;
            REQUIRE(!res.IsOne());
            res = small > small;
            REQUIRE(!res.IsOne());
            res = small >= small;
            REQUIRE(res.IsOne());
        }*/

        SECTION("Testing (non)equality operator") {
            Bvec<CuddManagerT> small = Bvec<CuddManagerT>::bvec_con(size, val, cudd_manager);
            Bvec<CuddManagerT> big = Bvec<CuddManagerT>::bvec_con(size, inv, cudd_manager);
            REQUIRE(!(small == big).IsOne());
            REQUIRE((small != big).IsOne());
            REQUIRE((big == big).IsOne());
            REQUIRE((small == small).IsOne());
            REQUIRE(!(small != small).IsOne());
            REQUIRE(!(big != big).IsOne());
        }
    }
}

TEST_CASE("Template Sylvan tests") {
    using namespace abstractBvec;
    using namespace helpers;
    initSylvan();
    SylvanManager sylvan_manager{};
    using SylvanManagerT = decltype(sylvan_manager);
    uint8_t val = 42U;
    uint8_t con = 5U;
    uint8_t inv = ~val;
    size_t size = 8U;
    uint8_t offset = 0U;
    uint8_t step = 2U;

    SECTION("Construction tests") {

        SECTION("Test false bitvector") {
            Bvec<SylvanManagerT> bvec_sylvan = Bvec<SylvanManagerT>::bvec_false(size, sylvan_manager);
            //compare with original?
        }


        SECTION("Test true bitvector") {
            Bvec<SylvanManagerT> bvec_sylvan = Bvec<SylvanManagerT>::bvec_true(size, sylvan_manager);
            //compare with original?
        }

        SECTION("Test constant bitvector") {
            Bvec<SylvanManagerT> bvec_sylvan = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            //compare with original?
        }

        SECTION("Test copy constructors") {
            Bvec<SylvanManagerT> bvec_sylvan = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> sylvan_copy (bvec_sylvan);
            Bvec<SylvanManagerT> sylvan_operator = bvec_sylvan;
            REQUIRE((bvec_sylvan == sylvan_copy).isOne());
            REQUIRE((bvec_sylvan == sylvan_operator).isOne());
            //compare with original?
        }

        SECTION("Test bvec_var") {
            int offset = 1;
            int step = 2;
            Bvec<SylvanManagerT> bvec_sylvan = Bvec<SylvanManagerT>::bvec_var(size, offset, step, sylvan_manager);
            //compare with original?
        }

        SECTION("Test bvec_varvec") {
            std::vector<int> var {0, 1, 2, 3, 4, 5, 6, 7};
            Bvec<SylvanManagerT> bvec_sylvan = Bvec<SylvanManagerT>::bvec_varvec(size, var.data(), sylvan_manager);
            //compare with original?
        }
    }

    SECTION("Logic operators") {

        SECTION("Test sylvan operation &/and") {
            Bvec<SylvanManagerT> x = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> y = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> res = x & y;
            Bvec<SylvanManagerT> res_inv = y & x;
            REQUIRE((res == y).isOne());
            REQUIRE((res == x).isOne());
            REQUIRE((res == res_inv).isOne());
        }

        SECTION("Test sylvan operation ^/xor") {
            Bvec<SylvanManagerT> x = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> y = Bvec<SylvanManagerT>::bvec_con(size, inv, sylvan_manager);
            Bvec<SylvanManagerT> res = x ^ y;
            Bvec<SylvanManagerT> res_inv = y ^ x;
            Bvec<SylvanManagerT> res_false = x ^ x;
            REQUIRE((res == Bvec<SylvanManagerT>::bvec_true(size, sylvan_manager)).isOne());
            REQUIRE((y != x).isOne());
            REQUIRE((res_inv == Bvec<SylvanManagerT>::bvec_true(size, sylvan_manager)).isOne());
            REQUIRE((res_false == Bvec<SylvanManagerT>::bvec_false(size, sylvan_manager)).isOne());
        }

        SECTION("Test sylvan operation |/or") {
            Bvec<SylvanManagerT> x = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> y = Bvec<SylvanManagerT>::bvec_con(size, inv, sylvan_manager);
            Bvec<SylvanManagerT> res = x | y;
            Bvec<SylvanManagerT> res_inv = y | x;
            REQUIRE((res == Bvec<SylvanManagerT>::bvec_true(size, sylvan_manager)).isOne());
            REQUIRE((y != x).isOne());
            REQUIRE((res_inv == Bvec<SylvanManagerT>::bvec_true(size, sylvan_manager)).isOne());
        }


        SECTION("Test sylvan operation !/not") {
            Bvec<SylvanManagerT> x = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> y = Bvec<SylvanManagerT>::bvec_con(size, inv, sylvan_manager);
            Bvec<SylvanManagerT> res = !x;
            Bvec<SylvanManagerT> res_inv = !y;
            REQUIRE((res == y).isOne());
            REQUIRE((res_inv == x).isOne());
            REQUIRE((x != y).isOne());
            res = !res;
            res_inv = !res_inv;
            REQUIRE((res == x).isOne());
            REQUIRE((res_inv == y).isOne());
        }

        SECTION("Test left/right shift operations") { //fix
            val = 44U;
            uint8_t bitshift = 2U;
            uint8_t l_shifted_val = val << bitshift;
            uint8_t r_shifted_val = val >> bitshift;
            Bvec<SylvanManagerT> bvec = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> l_shifted = Bvec<SylvanManagerT>::bvec_con(size, l_shifted_val, sylvan_manager);
            Bvec<SylvanManagerT> r_shifted = Bvec<SylvanManagerT>::bvec_con(size, r_shifted_val, sylvan_manager);
            Bvec<SylvanManagerT> shifting = Bvec<SylvanManagerT>::bvec_con(size, bitshift, sylvan_manager);
            Bvec<SylvanManagerT> res = bvec << shifting;
            REQUIRE((res == l_shifted).isOne());
            REQUIRE(res.bvec_val() == l_shifted_val);
            //res = res >> shifting;
            //REQUIRE((res == bvec).isOne());
            //REQUIRE(res.bvec_val() == val);

            Bvec<SylvanManagerT> fixed_res = bvec << bitshift;
            REQUIRE((fixed_res == l_shifted).isOne());
            REQUIRE(fixed_res.bvec_val() == l_shifted_val);
            fixed_res = fixed_res >> bitshift;
            REQUIRE((fixed_res == bvec).isOne());
            REQUIRE(fixed_res.bvec_val() == val);

            //res = bvec >> shifting;
            //REQUIRE((res == r_shifted).isOne());
            //REQUIRE(res.bvec_val() == r_shifted_val);
            //res = res << shifting;
            //REQUIRE((res == bvec).isOne());
            //REQUIRE(res.bvec_val() == val);

            fixed_res = bvec >> bitshift;
            REQUIRE((fixed_res == r_shifted).isOne());
            REQUIRE(fixed_res.bvec_val() == r_shifted_val);
            fixed_res = fixed_res << bitshift;
            REQUIRE(fixed_res.bvec_val() == val);
            REQUIRE((fixed_res == bvec).isOne());
        }
    }

    SECTION("Test arithmetic operators") {

        SECTION("Test +") {
            Bvec<SylvanManagerT> bvec = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> small = Bvec<SylvanManagerT>::bvec_con(size, con, sylvan_manager);
            Bvec<SylvanManagerT> res = bvec + small;
            Bvec<SylvanManagerT> res_inv = small + bvec;
            REQUIRE((res == res_inv).isOne());
            REQUIRE(res.bvec_val() == res_inv.bvec_val());
            REQUIRE(res.bvec_val() == (val + con));
            REQUIRE((res == Bvec<SylvanManagerT>::bvec_con(size, val + con, sylvan_manager)).isOne());
        }

        SECTION("Test -") {
            Bvec<SylvanManagerT> bvec = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> small = Bvec<SylvanManagerT>::bvec_con(size, con, sylvan_manager);
            Bvec<SylvanManagerT> res = bvec - small;
            Bvec<SylvanManagerT> added = res + small;
            REQUIRE(res.bvec_val() == (val - con));
            REQUIRE((bvec == added).isOne());
            REQUIRE((small == (added - res)).isOne());
            REQUIRE((res == Bvec<SylvanManagerT>::bvec_con(size, val - con, sylvan_manager)).isOne());
        }

        SECTION("Test * bitvector") {
            Bvec<SylvanManagerT> bvec = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> small = Bvec<SylvanManagerT>::bvec_con(size, con, sylvan_manager);
            Bvec<SylvanManagerT> res = bvec * small;
            Bvec<SylvanManagerT> res_inv = small * bvec;
            REQUIRE((res == res_inv).isOne());
            REQUIRE(res.bvec_val() == res_inv.bvec_val());
            REQUIRE(res.bvec_val() == (val * con));
            REQUIRE((res == Bvec<SylvanManagerT>::bvec_con(size + size, val * con, sylvan_manager)).isOne());
        }

        SECTION("Test * constant") {
            Bvec<SylvanManagerT> bvec = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> res = bvec * con;
            // Bvec<SylvanManagerT> res_inv = con * bvec; not possible
            // REQUIRE((res == res_inv).isOne());
            // REQUIRE(res.bvec_val() == res_inv.bvec_val());
            REQUIRE(res.bvec_val() == (val * con));
            REQUIRE((res == Bvec<SylvanManagerT>::bvec_con(size, val * con, sylvan_manager)).isOne());

        }

        SECTION("Test unsigned div") {
            Bvec<SylvanManagerT> bvec = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> div = Bvec<SylvanManagerT>::bvec_con(size, con, sylvan_manager);
            Bvec<SylvanManagerT> res{sylvan_manager};
            Bvec<SylvanManagerT> rem{sylvan_manager};
            Bvec<SylvanManagerT>::bvec_div(bvec, div, res, rem);
            REQUIRE(res.bvec_val() == (val / con));
            REQUIRE((res == Bvec<SylvanManagerT>::bvec_con(size, val / con, sylvan_manager)).isOne());
            REQUIRE(rem.bvec_val() == (val % con));
            REQUIRE((rem == Bvec<SylvanManagerT>::bvec_con(size, val % con, sylvan_manager)).isOne());
        }

        /*SECTION("Test signed div") { //fix
            Bvec<SylvanManagerT> bvec = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> div = Bvec<SylvanManagerT>::bvec_con(size, -5, sylvan_manager);
            Bvec<SylvanManagerT> res{sylvan_manager};
            Bvec<SylvanManagerT> rem{sylvan_manager};
            Bvec<SylvanManagerT>::bvec_sdiv(bvec, div, res, rem);
        }*/
    }

    SECTION("Test compare operators") {

        SECTION("Testing unsigned lth, lte operators") {
            Bvec<SylvanManagerT> small = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> big = Bvec<SylvanManagerT>::bvec_con(size, inv, sylvan_manager);
            REQUIRE((small < big).isOne());
            REQUIRE(!(big < small).isOne());
            REQUIRE((small <= big).isOne());
            REQUIRE(!(big <= small).isOne());
            REQUIRE((small <= small).isOne());
            REQUIRE(!(small < small).isOne());
        }

        SECTION("Testing unsigned gth, gte operators") {
            Bvec<SylvanManagerT> small = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> big = Bvec<SylvanManagerT>::bvec_con(size, inv, sylvan_manager);
            REQUIRE(!(small > big).isOne());
            REQUIRE((big > small).isOne());
            REQUIRE(!(small >= big).isOne());
            REQUIRE((big >= small).isOne());
            REQUIRE((small >= small).isOne());
            REQUIRE(!(small > small).isOne());
        }

        /*SECTION("Testing signed lth, lte operators") {
            Bvec<SylvanManagerT> small = Bvec<SylvanManagerT>::bvec_con(size, -84, sylvan_manager);
            Bvec<SylvanManagerT> big = Bvec<SylvanManagerT>::bvec_con(size, -42, sylvan_manager);
            sylvan::Bdd res = small < big;
            REQUIRE(res.isOne());
            res = small <= big;
            REQUIRE(res.isOne());
            res = big <= small;
            REQUIRE(!res.isOne());
            res = big < small;
            REQUIRE(!res.isOne());
            res = small > small;
            REQUIRE(!res.isOne());
            res = small >= small;
            REQUIRE(res.isOne());
        }

        SECTION("Testing signed gth, gte operators") {
            Bvec<SylvanManagerT> small = Bvec<SylvanManagerT>::bvec_con(size, -84, sylvan_manager);
            Bvec<SylvanManagerT> big = Bvec<SylvanManagerT>::bvec_con(size, -42, sylvan_manager);
            sylvan::Bdd res = small < big;
            REQUIRE(res.isOne());
            res = small <= big;
            REQUIRE(res.isOne());
            res = big <= small;
            REQUIRE(!res.isOne());
            res = big < small;
            REQUIRE(!res.isOne());
            res = small > small;
            REQUIRE(!res.isOne());
            res = small >= small;
            REQUIRE(res.isOne());
        }*/

        SECTION("Testing (non)equality operator") {
            Bvec<SylvanManagerT> small = Bvec<SylvanManagerT>::bvec_con(size, val, sylvan_manager);
            Bvec<SylvanManagerT> big = Bvec<SylvanManagerT>::bvec_con(size, inv, sylvan_manager);
            REQUIRE(!(small == big).isOne());
            REQUIRE((small != big).isOne());
            REQUIRE((big == big).isOne());
            REQUIRE((small == small).isOne());
            REQUIRE(!(small != small).isOne());
            REQUIRE(!(big != big).isOne());
        }
    }

    closeSylvan();
}
