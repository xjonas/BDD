#include <cuddObj.hh>
#include <sylvan_obj.hpp>

/*
    Needed methods for new manager

    typdef BDD; must be specified

    Returning new BDD constant true;
    bddOne() const;

    Returning new BDD constant false;
    bddZero() const;

    Returning new BDD variable with position @param pos;
    bddVar(int pos) const;

    Returning true if @param bdd is a constant, false otherwise.
    bool
    isConst(const Bdd& bdd) const;

    Return true if @param bdd is a true constant, false otherwise.
    bool
    isOne(const Bdd& bdd) const;

    Return true if @param bdd is a false constant, false otherwise.
    bool
    isZero(const Bdd& bdd) const;
*/

class CuddManager {
private:

    const Cudd& m_manager;

public:

    typedef BDD BDD;

    CuddManager(const Cudd& manager) : m_manager(manager) {}

    ~CuddManager() = default;

    BDD
    bddOne() const {
        return m_manager.bddOne();
    }

    BDD
    bddZero() const {
        return m_manager.bddZero();
    }

    BDD
    bddVar(int pos) const {
        return m_manager.bddVar(pos);
    }

    bool
    isConst(const BDD& bdd) const {
        return (bdd.IsOne() || bdd.IsZero());
    }

    bool
    isOne(const BDD& bdd) const {
        return bdd.IsOne();
    }

    bool
    isZero(const BDD& bdd) const {
        return bdd.IsZero();
    }

};

class SylvanManager {
public:
    typedef sylvan::Bdd BDD;

    SylvanManager() = default;

    ~SylvanManager() = default;

    sylvan::Bdd
    bddOne() const {
        return sylvan::Bdd::bddOne();
    }

    sylvan::Bdd
    bddZero() const {
        return sylvan::Bdd::bddZero();
    }

    sylvan::Bdd
    bddVar(int pos) const {
        return sylvan::Bdd::bddVar(pos);
    }

    bool
    isConst(const sylvan::Bdd& bdd) const {
        return bdd.isConstant();
    }

    bool
    isOne(const sylvan::Bdd& bdd) const {
        return bdd.isOne();
    }

    bool
    isZero(const sylvan::Bdd& bdd) const {
        return bdd.isZero();
    }

};
