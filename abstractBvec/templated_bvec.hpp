#ifndef BDD_TEMPLATE_H
#define BDD_TEMPLATE_H

#include <functional>
#include <fstream>
#include <vector>

namespace abstractBvec {

template<class ManagerT>
class Bvec {
    using TBDD = typename ManagerT::BDD;
    std::vector<TBDD> m_bitvec;
    const ManagerT& m_manager;

public:

    Bvec(const ManagerT& manager) : m_bitvec(0), m_manager(manager) {};

    Bvec(size_t bitnum, bool isTrue, const ManagerT& manager) : m_bitvec(0), m_manager(manager) {
        TBDD value;
        if (isTrue) {
            value = m_manager.bddOne();
        } else {
            value = m_manager.bddZero();
        }
        m_bitvec.resize(bitnum, value);
    }

    Bvec(const Bvec& o) : m_bitvec(o.m_bitvec), m_manager(o.m_manager) {
        /*if (o.bitnum() == 0)
        {
            //DEFAULT(dst);
            //nejak to lognut alebo vyhodit vynimku
        }

        for (size_t i = 0; i < o.bitnum(); ++i) {
            m_bitvec[i] = o[i];
        }*/
    }

    Bvec& operator=(Bvec other) {
        swap(other);
        return *this;
    }

    ~Bvec() = default;

    void
    set(size_t i, const TBDD& con) {
        m_bitvec.at(i) = con;
    }

    TBDD&
    operator[](size_t i) {
        return m_bitvec.at(i);
    }

    const TBDD&
    operator[](size_t i) const {
        return m_bitvec.at(i);
    }

    size_t
    bitnum() const {
        return m_bitvec.size();
    }

    bool
    empty() const {
        return m_bitvec.empty();
    }

    static Bvec
    bvec_build(size_t bitnum, bool isTrue, const ManagerT& manager) {
        Bvec res(bitnum, isTrue, manager);
        return res;
    }

    static Bvec
    bvec_true(size_t bitnum, const ManagerT& manager) {
        return bvec_build(bitnum, true, manager);
    }

    static Bvec
    bvec_false(size_t bitnum, const ManagerT& manager) {
        return bvec_build(bitnum, false, manager);
    }

    static Bvec
    bvec_con(size_t bitnum, int val, const ManagerT& manager) {
        Bvec res = reserve(bitnum, manager);
        for (size_t i = 0; i < bitnum; ++i) {
            if (val & 1U) {
                res.m_bitvec.push_back(manager.bddOne());
            } else {
                res.m_bitvec.push_back(manager.bddZero());
            }
            val >>= 1U;
        }
        return res;
    }

    static Bvec
    bvec_var(size_t bitnum, int offset, int step, const ManagerT& manager) {
        Bvec res = bvec_false(bitnum, manager);
        for (int i = 0; i < bitnum; ++i) {
            res[i] = manager.bddVar(offset + i * step);
        }

        return res;
    }

    static Bvec
    bvec_varvec(size_t bitnum, int *var, const ManagerT& manager) {
        Bvec res = reserve(bitnum, manager);
        for (size_t i = 0; i < bitnum; ++i) {
            res.m_bitvec.push_back(manager.bddVar(var[i]));
        }

        return res;
    }

    Bvec
    bvec_coerce(size_t bits) const {
        Bvec res = bvec_false(bits, m_manager);
        size_t minnum = std::min(bits, bitnum());
        for (size_t i = 0; i < minnum; ++i) {
            res[i] = m_bitvec[i];
        }
        return res;
    }

    bool
    bvec_isConst() const {
        for (size_t i = 0; i < bitnum(); ++i) {
            if (!(m_manager.isConst(m_bitvec[i]))) {
                return false;
            }
        }
        return true;
    }

    int
    bvec_val() const {
        int val = 0;
        for (size_t i = bitnum(); i >= 1U; --i) {
            if (m_manager.isOne(m_bitvec[i - 1U]))
                val = (val << 1) | 1;
            else if (m_manager.isZero(m_bitvec[i - 1U]))
                val = val << 1;
            else
                return 0;
        }
        return val;
    }

    static Bvec
    bvec_copy(const Bvec& other) {
        return Bvec(other);
    }

    static Bvec
    bvec_map1(const Bvec& src, std::function<TBDD(const TBDD&)> fun) {
        Bvec res = reserve(src.bitnum(), src.m_manager);
        for (size_t i =0; i < src.bitnum(); ++i) {
            res.m_bitvec.push_back(fun(src[i]));
        }
        return res;
    }

    static Bvec
    bvec_map2(const Bvec& first, const Bvec& second, std::function<TBDD(const TBDD&, const TBDD&)> fun) {
        Bvec res(first.m_manager);

        //DEFAULT(res);
        if (first.bitnum() != second.bitnum()) {
            //bdd_error(BVEC_SIZE);
            return res;
        }

        reserve(res, first.bitnum());
        for (size_t i = 0; i < first.bitnum(); ++i) {
            res.m_bitvec.push_back(fun(first[i], second[i]));
        }
        return res;
    }

    static Bvec
    bvec_map3(const Bvec& first, const Bvec& second, const Bvec& third, std::function<TBDD(const TBDD&, const TBDD&, const TBDD&)> fun) {
        Bvec res(first.m_manager);

        //DEFAULT(res);
        if (first.bitnum() != second.bitnum() || second.bitnum() != third.bitnum()) {
            //bdd_error(BVEC_SIZE);
            return res;
        }

        reserve(res, first.bitnum());
        for (size_t i = 0; i < first.bitnum(); ++i) {
            res.m_bitvec.push_back(fun(first[i], second[i], third[i]));
        }
        return res;
    }

    static Bvec
    bvec_add(const Bvec& left, const Bvec& right) {
        Bvec res(left.m_manager);
        TBDD comp = res.m_manager.bddZero();

        if (left.bitnum() == 0 || right.bitnum() == 0) {
            //throw
            return res;
        }

        if (left.bitnum() != right.bitnum()) {
            //bdd_error(BVEC_SIZE);
            //throw
            return res;
        }

        reserve(res, left.bitnum());

        for (size_t i = 0; i < left.bitnum(); ++i) {

            /* bitvec[i] = l[i] ^ r[i] ^ c; */
            res.m_bitvec.push_back((left[i] ^ right[i]) ^ comp);

            /* c = (l[i] & r[i]) | (c & (l[i] | r[i])); */
            comp = (left[i] & right[i]) | (comp & (left[i] | right[i]));
        }
        return res;
    }

    static Bvec
    bvec_sub(const Bvec& left, const Bvec& right) {
        Bvec res(left.m_manager);
        TBDD comp = res.m_manager.bddZero();

        if (left.bitnum() == 0 || right.bitnum() == 0) {
            //DEFAULT(res);
            return res;
        }

        if (left.bitnum() != right.bitnum()) {
            //bdd_error(BVEC_SIZE);
            //DEFAULT(res);
            return res;
        }

        reserve(res, left.bitnum());

        for (size_t i = 0; i < left.bitnum(); ++i) {

            /* bitvec[n] = l[n] ^ r[n] ^ comp; */
            res.m_bitvec.push_back((left[i] ^ right[i]) ^ comp);
            /* comp = (l[n] & r[n] & comp) | (!l[n] & (r[n] | comp)); */
            comp = (left[i] & right[i] & comp) | (~left[i] & (right[i] | comp));
        }

        return res;
    }

    Bvec
    bvec_mulfixed(int con) const {
        Bvec res(m_manager);

        if (bitnum() == 0) {
            //DEFAULT(res);
            return res;
        }

        if (con == 0) {
            return bvec_false(bitnum(), m_manager);
        }

        Bvec next = bvec_false(bitnum(), m_manager);
        for (size_t i = 1; i < bitnum(); ++i) {
            next[i] = m_bitvec[i - 1];
        }

        Bvec rest = next.bvec_mulfixed(con >> 1);

        if (con & 0x1) {
            res = bvec_add(*this, rest);
        } else {
            res = rest;
        }
        return res;
    }

    static Bvec
    bvec_mul(const Bvec& left, const Bvec& right) {
        size_t bitnum = left.bitnum() + right.bitnum();
        Bvec res = bvec_false(bitnum, left.m_manager);

        if (left.bitnum() == 0 || right.bitnum() == 0) {
            //DEFAULT(res);
            return res;
        }
        Bvec leftshifttmp = Bvec(left);
        Bvec leftshift = leftshifttmp.bvec_coerce(bitnum);

        for (size_t i = 0; i < right.bitnum(); ++i) {
            Bvec added = bvec_add(res, leftshift);

            for (size_t m = 0; m < bitnum; ++m) {
                res[m] = right[i].Ite(added[m], res[m]);
            }

            /* Shift 'leftshift' one bit left */
            for (size_t m = bitnum - 1; m >= 1; --m) {
                leftshift[m] = leftshift[m - 1];
            }

            //leftshift.m_bitvec.resize(leftshift.bitnum() - 1);
            leftshift[0] = res.m_manager.bddZero();
        }
        return res;
    }

    int
    bvec_divfixed(size_t con, Bvec& result, Bvec& rem) const {
        if (con > 0) {
            Bvec divisor = bvec_con(bitnum(), con, m_manager);
            Bvec tmp = bvec_false(bitnum(), m_manager);
            Bvec tmpremainder = tmp.bvec_shlfixed(1, m_bitvec[bitnum() - 1]);
            Bvec res = bvec_shlfixed(1, m_manager.bddZero());

            bvec_div_rec(divisor, tmpremainder, result, divisor.bitnum());
            Bvec remainder = tmpremainder.bvec_shrfixed(1, m_manager.bddZero());

            result = res;
            rem = remainder;
            return 0;
        }
        return 1; //div by zero TODO find error codes
        //return bdd_error(BVEC_DIVZERO);
    }

    static int
    bvec_div(const Bvec& left, const Bvec& right, Bvec& result, Bvec& remainder) {
        size_t bitnum = left.bitnum() + right.bitnum();
        if (left.bitnum() == 0 || right.bitnum() == 0 || left.bitnum() != right.bitnum()) {
            //return bdd_error(BVEC_SIZE);
            return 1;
        }

        Bvec rem = left.bvec_coerce(bitnum);
        Bvec divtmp = right.bvec_coerce(bitnum);
        Bvec div = divtmp.bvec_shlfixed(left.bitnum(), right.m_manager.bddZero());

        Bvec res = bvec_false(right.bitnum(), right.m_manager);

        for (size_t i = 0; i < right.bitnum() + 1; ++i) {
            TBDD divLteRem = bvec_lte(div, rem);
            Bvec remSubDiv = bvec_sub(rem, div);

            for (size_t j = 0; j < bitnum; ++j) {
                rem[j] = divLteRem.Ite(remSubDiv[j], rem[j]);
            }

            if (i > 0) {
                res[right.bitnum() - i] = divLteRem;
            }

            /* Shift 'div' one bit right */
            for (size_t j = 0; j < bitnum - 1; ++j) {
                div[j] = div[j + 1];
            }
            div[bitnum - 1] = res.m_manager.bddZero();
        }

        result = res;
        remainder = rem.bvec_coerce(right.bitnum());
        return 0;
    }

    static int
    bvec_sdiv(const Bvec& left, const Bvec& right, Bvec& result, Bvec& rem) {
        if (left.bitnum() == 0 || right.bitnum() == 0 || left.bitnum() != right.bitnum()) {
            //return bdd_error(BVEC_SIZE);
            return 1;
        }
        TBDD sign = bdd_xor(left.m_bitvec.back(), right.m_bitvec.back());
        Bvec linv = !left + bvec_con(left.bitnum(), 1U, left.m_manager);
        Bvec rinv = !right + bvec_con(right.bitnum(), 1U, left.m_manager);;
        int res = bvec_div(linv, rinv, result, rem);
        result.m_bitvec.back() = sign;
        return res;
    }

    static Bvec
    bvec_ite(const TBDD& val, const Bvec& left, const Bvec& right) {
        Bvec res(left.m_manager);

        if (left.bitnum() != right.bitnum()) {
            //bdd_error(BVEC_SIZE);
            return res;
        }
        reserve(res, left.bitnum(), left.m_manager);
        for (size_t i = 0; i < left.bitnum(); ++i) {
            res.m_bitvec.push_back(val.Ite(left[i], right[i]));
        }
        return res;
    }

    Bvec
    bvec_shlfixed(int pos, const TBDD& con) const {
        Bvec res(m_manager);
        size_t min = bitnum() < pos ? bitnum() : pos;

        if (pos < 0U) {
            //bdd_error(BVEC_SHIFT);
            //DEFAULT(res);
            return res;
        }
        if (bitnum() == 0) {
            //DEFAULT(res);
            return res;
        }

        reserve(res, bitnum());
        for (size_t i = 0; i < min; ++i) {
            res.m_bitvec.push_back(con);
        }
        for (size_t i = min; i < bitnum(); i++) {
            res.m_bitvec.push_back(m_bitvec[i - pos]);
        }
        return res;
    }

    static Bvec
    bvec_shl(const Bvec& left, const Bvec& right, const TBDD& con) {
        Bvec res(left.m_manager);
        if (left.bitnum() == 0 || right.bitnum() == 0) {
            //DEFAULT(res);
            return res;
        }

        res = bvec_false(left.bitnum(), left.m_manager);

        for ( size_t i = 0; i <= left.bitnum(); ++i) {
            Bvec val = bvec_con(right.bitnum(), i, left.m_manager);
            TBDD rEquN = bvec_equ(right, val);

            for ( size_t j = 0; j < left.bitnum(); ++j) {
                TBDD tmp1;
                /* Set the m'th new location to be the (m+n)'th old location */
                if (static_cast<int>(j - i) >= 0) {
                    tmp1 = rEquN & left[j - i];
                } else {
                    tmp1 = rEquN & con;
                }
                    res[j] = res[j] | tmp1;
                }
        }

        /* At last make sure 'c' is shiftet in for r-values > l-bitnum */
        Bvec val = bvec_con(right.bitnum(), left.bitnum(), left.m_manager);
        TBDD rEquN = bvec_gth(right, val);

        for (size_t i = 0; i < left.bitnum(); i++) {
            res[i] |= (rEquN & con);
        }
        return res;
    }

    Bvec
    bvec_shrfixed(int pos, const TBDD& con) const {
        Bvec res(m_manager);

        if (pos < 0) {
            //bdd_error(BVEC_SHIFT);
            //DEFAULT(res);
            return res;
        }

        if (bitnum() == 0) {
            //DEFAULT(res);
            return res;
        }
        int maxnum = std::max(static_cast<int>(bitnum() - pos), 0);
        res = bvec_false(bitnum(), res.m_manager);

        for (size_t i = static_cast<unsigned>(maxnum); i < bitnum(); ++i) {
            res[i] = con;
        }

        for (size_t i = 0; i < maxnum; ++i) {
            res[i] = m_bitvec[i + pos];
        }
        return res;
    }

    static Bvec
    bvec_shr(const Bvec& left, const Bvec& right, const TBDD& con) {
        Bvec res(left.m_manager);
        if (left.bitnum() == 0 || right.bitnum() == 0) {
            //DEFAULT(res);
            return res;
        }

        res = bvec_false(left.bitnum(), res.m_manager);
        TBDD tmp1, rEquN;

        for ( size_t i = 0; i <= left.bitnum(); ++i) {
            Bvec val = bvec_con(right.bitnum(), i, res.m_manager);
            rEquN = right == val;

            for (size_t j = 0; j < left.bitnum(); ++j) {
                /* Set the m'th new location to be the (m+n)'th old location */
                if (j + i <= 2)
                    tmp1 = rEquN & left[j + i];
                else
                    tmp1 = rEquN & con;
                res[j] = res[j] | tmp1;
            }
        }

        /* At last make sure 'c' is shiftet in for r-values > l-bitnum */
        Bvec val = bvec_con(right.bitnum(), left.bitnum(), res.m_manager);
        rEquN = bvec_gth(right, val);
        tmp1 = rEquN & con;

        for ( size_t i = 0; i < left.bitnum(); ++i) {
            res[i] = res[i] | tmp1;
        }
        return res;
    }

    static TBDD
    bvec_lth(const Bvec& left, const Bvec& right) {
        TBDD p = left.m_manager.bddZero();

        if (left.bitnum() == 0 || right.bitnum() == 0) {
            return p;
        }

        if (left.bitnum() != right.bitnum()) {
            //bdd_error(BVEC_SIZE);
            return p;
        }

        for (size_t i = 0; i < left.bitnum(); ++i) {
            /* p = (!l[n] & r[n]) |
             *     bdd_apply(l[n], r[n], bddop_biimp) & p; */

            p = left[i].Ite(left.m_manager.bddZero(), right[i]) |
                (bdd_biimpl(left.m_manager, left[i], right[i]) & p);
        }

        return p;
    }

    static TBDD
    bvec_lte(const Bvec& left, const Bvec& right) {
        TBDD p = left.m_manager.bddOne();

        if (left.bitnum() == 0 || right.bitnum() == 0) {
            return left.m_manager.bddZero();
        }

        if (left.bitnum() != right.bitnum()) {
            //bdd_error(BVEC_SIZE);
            return p; //alebo tu zero?
        }

        for (size_t i = 0; i < left.bitnum(); ++i) {
            /* p = (!l[n] & r[n]) |
             *     bdd_apply(l[n], r[n], bddop_biimp) & p; */
            p = left[i].Ite(left.m_manager.bddZero(), right[i]) |
                (bdd_biimpl(left.m_manager, left[i], right[i]) & p);
        }

        return p;
    }

    static TBDD
    bvec_gth(const Bvec& left, const Bvec& right) {
        return !bvec_lte(left, right);
    }

    static TBDD
    bvec_gte(const Bvec& left, const Bvec& right) {
        return !bvec_lth(left, right);
    }

    static TBDD
    bvec_slth(const Bvec& left, const Bvec& right) {
        TBDD p = left.m_manager.bddZero();

        if (left.bitnum() == 0 || right.bitnum() == 0) {
            return p;
        }

        if (left.bitnum() != right.bitnum()) {
            //bdd_error(BVEC_SIZE);
            return p;
        }

        if (left.m_bitvec.back() < right.m_bitvec.back()) {
            return left.m_manager.bddZero();
        } else if (left.m_bitvec.back() > right.m_bitvec.back()) {
            return left.m_manager.bddOne();
        } else if (left.m_bitvec.back() == left.m_manager.bddZero()) {
            Bvec linv = !left + bvec_con(left.bitnum(), 1U, left.m_manager);
            Bvec rinv = !right + bvec_con(right.bitnum(), 1U, left.m_manager);
            return bvec_lth(linv, rinv);
            //return bvec_lth(left, right);
        } else {
            Bvec linv = !left + bvec_con(left.bitnum(), 1U, left.m_manager);
            Bvec rinv = !right + bvec_con(right.bitnum(), 1U, left.m_manager);
            return bvec_lth(linv, rinv);
        }
    }

    static TBDD
    bvec_slte(const Bvec& left, const Bvec& right) {
        if (left.bitnum() == 0 || right.bitnum() == 0) {
            return left.m_manager.bddZero();
        }

        if (left.bitnum() != right.bitnum()) {
            //bdd_error(BVEC_SIZE);
            return left.m_manager.bddOne(); //alebo tu ma byt zero?
        }

        if (left.m_bitvec.back() < right.m_bitvec.back()) {
            return left.m_manager.bddZero();
        } else if (left.m_bitvec.back() > right.m_bitvec.back()) {
            return left.m_manager.bddOne();
        } else if (left.m_bitvec.back() == left.m_manager.bddZero()) {
            Bvec linv = !left + bvec_con(left.bitnum(), 1U, left.m_manager);
            Bvec rinv = !right + bvec_con(right.bitnum(), 1U, left.m_manager);;
            return bvec_lte(linv, rinv);
        } else {
            Bvec linv = !left + bvec_con(left.bitnum(), 1U, left.m_manager);
            Bvec rinv = !right + bvec_con(right.bitnum(), 1U, left.m_manager);;
            return bvec_lte(linv, rinv);
        }
    }

    static TBDD
    bvec_sgth(const Bvec& left, const Bvec& right) {
        return !bvec_slte(left, right);
    }

    static TBDD
    bvec_sgte(const Bvec& left, const Bvec& right) {
        return !bvec_slth(left, right);
    }

    static TBDD
    bvec_equ(const Bvec& left, const Bvec& right) {
        TBDD p = left.m_manager.bddOne();

        if (left.bitnum() == 0 || right.bitnum() == 0) {
            return left.m_manager.bddZero();
        }

        if (left.bitnum() != right.bitnum()) {
            //bdd_error(BVEC_SIZE);
            return left.m_manager.bddZero();
        }

        for (size_t i = 0; i < left.bitnum(); ++i) {
            p &= bdd_biimpl(left.m_manager, left[i], right[i]);
        }
        return p;
    }

    Bvec
    operator&(const Bvec& other) const { return bvec_map2(*this, other, bdd_and); }

    Bvec
    operator^(const Bvec& other) const { return bvec_map2(*this, other, bdd_xor); }

    Bvec
    operator|(const Bvec& other) const { return bvec_map2(*this, other, bdd_or); }

    Bvec
    operator!(void) const { return bvec_map1(*this, bdd_not); }

    Bvec
    operator<<(int con) const { return bvec_shlfixed(con, m_manager.bddZero()); }

    Bvec
    operator<<(const Bvec& other) const { return bvec_shl(*this, other, m_manager.bddZero()); }

    Bvec
    operator>>(int con) const { return bvec_shrfixed(con, m_manager.bddZero()); }

    Bvec
    operator>>(const Bvec& other) const { return bvec_shr(*this, other, m_manager.bddZero()); }

    Bvec
    operator+(const Bvec& other) const { return bvec_add(*this, other); }

    Bvec
    operator-(const Bvec& other) { return bvec_sub(*this, other); }

    Bvec
    operator*(int con) const { return bvec_mulfixed(con); }

    Bvec
    operator*(const Bvec& other) const { return bvec_mul(*this, other); }

    TBDD
    operator<(const Bvec& other) const { return bvec_lth(*this, other); }

    TBDD
    operator<=(const Bvec& other) const { return bvec_lte(*this, other); }

    TBDD
    operator>(const Bvec& other) const { return bvec_gth(*this, other); }

    TBDD
    operator>=(const Bvec& other) const { return bvec_gte(*this, other); }

    TBDD
    operator==(const Bvec& other) const { return bvec_equ(*this, other); }

    TBDD
    operator!=(const Bvec& other) const { return !(*this == other); }

private:

    static void
    bvec_div_rec(Bvec& divisor, Bvec& remainder, Bvec& result, size_t step) {
        TBDD isSmaller = bvec_lte(divisor, remainder);
        Bvec newResult = result.bvec_shlfixed(1, isSmaller);
        Bvec zero = bvec_build(divisor.bitnum(), false, result.m_manager);
        Bvec sub = reserve(divisor.bitnum(), result.m_manager);

        for (size_t i = 0; i < divisor.bitnum(); ++i) {
            sub.m_bitvec.push_back(isSmaller.Ite(divisor[i], zero[i]));
        }

        Bvec tmp = remainder - sub;
        Bvec newRemainder = tmp.bvec_shlfixed(1, result[divisor.bitnum() - 1]);

        if (step > 1) {
            bvec_div_rec(divisor, newRemainder, newResult, step - 1);
        }

        result = newResult;
        remainder = newRemainder;
    }

    static TBDD
    bdd_and(const TBDD& first, const TBDD& second) {
        return first & second;
    }

    static TBDD
    bdd_xor(const TBDD& first, const TBDD& second) {
        return first ^ second;
    }

    static TBDD
    bdd_or(const TBDD& first, const TBDD& second) {
        return first | second;
    }

    static TBDD
    bdd_not(const TBDD& src) {
        return !src;
    }

    static TBDD
    bdd_biimpl(const ManagerT& manager, const TBDD& first, const TBDD& second) {
        return first.Ite(second, manager.bddOne()) & second.Ite(first, manager.bddOne());
    }

    void
    swap(Bvec& other) {
        using std::swap;
        swap(m_bitvec, other.m_bitvec);
    }

    static Bvec
    reserve(size_t bitnum, const ManagerT& manager) {
        Bvec res(manager);
        res.m_bitvec.reserve(bitnum);
        return res;
    }

    static void
    reserve(Bvec& bitvector, size_t bitnum) {
        bitvector.m_bitvec.reserve(bitnum);
    }
};

} //abstractBvec namespace

#endif //BDD_TEMPLATE_H
