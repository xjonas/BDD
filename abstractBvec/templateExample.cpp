#include "templated_bvec.hpp"
#include "managers.h"

int main(int argc, char ** argv) {
    using namespace abstractBvec;
    Cudd manager;
    CuddManager cudd_manager(manager);

    using Bvec_cudd = Bvec<decltype(cudd_manager)>;
    Bvec_cudd cudd_bvec =
        Bvec_cudd::bvec_con(8, 42, cudd_manager);

    SylvanManager sylvan_manager{};
    using Bvec_syl = Bvec<decltype(sylvan_manager)>;
    Bvec_syl sylvan_bvec =
        Bvec_syl::bvec_var(8, 0, 1, sylvan_manager);

    return 0;
}
