This library contains the implementation of bit-vectors for packages Cudd and Sylvan.

How to compile this library:

1. Download the Cudd package. For example, use the download link [here](vlsi.colorado.edu/~fabio/). You can find there also documentation.
2. Unzip the package and to install Cudd use command:
  './configure CC=clang CXX=clang++ --enable-silent-rules \
  --enable-shared --enable-obj && \
  make -j4 check && \
  make install'
  You may need to use 'sudo make install'
3. Next, you have to run: 'git clone https://github.com/trolando/sylvan.git'
4. To build Sylvan run:
  'mkdir build && \
  cd build && \
  cmake .. && \
  make && make test && make install'
  You may need to use 'sudo make install'
5. The last package you need to build is BuDDy. You can download it [here](https://sourceforge.net/projects/buddy/?source=directory).
6. Build this package using:
'./configure && \
  make && \
  make install'
  You may need to use 'sudo make install'
7. Afterwards, you should be able to build this library. In the root folder of the repository run:
  'mkdir build && \
  cd build && \
  cmake .. && make'
