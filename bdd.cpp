//
// Created by pnavratil on 6/18/17.
//
#include <iostream>
#include "bvec.h"
// #include "bvec_cudd.h"


int main(int argc, char ** argv) {
    std::cout << "Hello World!" << std::endl;
    bdd_init(1000, 1000);
    bdd_setvarnum(100);

    bdd a = bdd_ithvar(0) ^ bdd_ithvar(1);
    bdd b = bdd_ithvar(0) | bdd_ithvar(2);
    bdd res = a & b;

    bdd_printdot(a);
    bdd_printdot(b);
    bdd_printdot(res);

    /*bvec x = bvec_var(4, 0, 1);
    bvec y = bvec_con(4, 5);
    bvec expected = bvec_con(4, 5);
    bvec result = x & y;
    for (size_t i = 0; i < 4; ++i) {
        bdd_printdot(result[i]);
        std::cout << std::endl;
    }*/

    //bdd res = expected == result;
    /*if (res == bddtrue)
        std::cout << "true" << std::endl;
    else
        std::cout << "false" << std::endl;*/
    bdd_done();
    std::cout << "_________________________" << std::endl;
    // using namespace cudd;
    // Cudd manager;
    // Bvec cuddX = Bvec::bvec_con(manager, 4U, 2);
    // Bvec cuddY = Bvec::bvec_con(manager, 4U, 5);
    // Bvec cudd_res = cuddX & cuddY;

    return 0;
}
