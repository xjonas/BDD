#include <iostream>
#include <string>
#include <fstream>

struct Results {
    Results(const std::string& name) : name(name) {}

    friend std::ostream& operator<< (std::ostream& stream, Results& res) {
        stream << "Name: " << res.name << ", sat: " << (unsigned) res.sat;
        stream << ", unsat: " << (unsigned) res.unsat << ", error: ";
        stream << (unsigned) res.error << ", timeout: " << (unsigned) res.timeout;
        stream << ", time: " << std::to_string(res.time) << std::endl;
        res.sat = 0;
        res.unsat = 0;
        res.error = 0;
        res.timeout = 0;
        res.time = 0.0;
        return stream;
    }

    std::string name;
    unsigned sat = 0;
    unsigned unsat = 0;
    unsigned error = 0;
    unsigned timeout = 0;
    double time = 0.0;
};

class Parser {
    Results buddy;
    Results cudd;
    Results sylvan;

public:
    Parser() : buddy("buddy"), cudd("cudd"), sylvan("sylvan") {}

    void parseFile(const std::string& fileName) {
        std::ifstream input(fileName);
        if (!input) {
            std::cerr << "File " << fileName << " not found" << std::endl;
        }
        std::string line;
        bool isFirst = true;
        while (getline(input, line)) {
            if (isFirst) {
                isFirst = false;
                continue;
            }
            for (size_t i = 0; i < 13; ++i) {
                size_t pos = line.find(",");
                std::string substr = line.substr(0, pos);
                line = line.substr(pos + 1);
                if (i == 1) {
                    if (substr == "sat") {
                        ++buddy.sat;
                    } else if (substr == "unsat") {
                        ++buddy.unsat;
                    } else if (substr == "timeout"){
                        ++buddy.timeout;
                    } else {
                        ++buddy.error;
                    }
                } else if (i == 2) {
                    double res = std::stof(substr);
                    buddy.time += res;
                } else if (i == 5) {
                    if (substr == "sat") {
                        ++cudd.sat;
                    } else if (substr == "unsat") {
                        ++cudd.unsat;
                    } else if (substr == "timeout"){
                        ++cudd.timeout;
                    } else {
                        ++cudd.error;
                    }
                } else if (i == 6) {
                    double res = std::stof(substr);
                    cudd.time += res;
                } else if (i == 9) {
                    if (substr == "sat") {
                        ++sylvan.sat;
                    } else if (substr == "unsat") {
                        ++sylvan.unsat;
                    } else if (substr == "timeout"){
                        ++sylvan.timeout;
                    } else {
                        ++sylvan.error;
                    }
                } else if (i == 7) {
                    double res = std::stof(substr);
                    sylvan.time += res;
                }
            }
        }
        std::cout << fileName << std::endl << buddy << cudd << sylvan;
    }

};

int main(int argc, char ** argv) {
    Parser pars;
    pars.parseFile("lnira.csv");
    pars.parseFile("smtlib.csv");
    pars.parseFile("symdivine.csv");
    return 0;
}
